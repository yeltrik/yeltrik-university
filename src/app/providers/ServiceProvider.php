<?php

namespace Yeltrik\University\app\providers;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if ( file_exists(config_path('yeltrik-university-database-connections.php')) ) {
            // User Customizable
            $this->mergeConfigFrom(
                config_path('yeltrik-university-database-connections.php'), 'database.connections'
            );
            //dd(config('database'));
        } else {
            // Non Customizable
            $this->mergeConfigFrom(
                __DIR__ . '/../../config/database-connections.php', 'database.connections'
            );
            //dd(config('database'));
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'university');
        $this->loadViewsFrom(__DIR__ . '/../../../../../../resources/views', 'project');

        if ($this->app->runningInConsole()) {
            $this->publishResources();
        }
    }

    protected function publishResources()
    {
        // User Customizable
        $this->publishes([
            __DIR__ . '/../../config/database-connections.php' => config_path('yeltrik-university-database-connections.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../../database/seeders/CollegeSeeder.php' => database_path('seeders/CollegeSeeder.php'),
            __DIR__ . '/../../database/seeders/DeanSeeder.php' => database_path('seeders/DeanSeeder.php'),
            __DIR__ . '/../../database/seeders/DepartmentHeadSeeder.php' => database_path('seeders/DepartmentHeadSeeder.php'),
            __DIR__ . '/../../database/seeders/DepartmentSeeder.php' => database_path('seeders/DepartmentSeeder.php'),
            __DIR__ . '/../../database/seeders/FacultySeeder.php' => database_path('seeders/FacultySeeder.php'),
            __DIR__ . '/../../database/seeders/SemesterSeeder.php' => database_path('seeders/SemesterSeeder.php'),
            __DIR__ . '/../../database/seeders/StaffSeeder.php' => database_path('seeders/StaffSeeder.php'),
            __DIR__ . '/../../database/seeders/WkuIdentitySeeder.php' => database_path('seeders/WkuIdentitySeeder.php'),
        ], 'university');

    }

}
