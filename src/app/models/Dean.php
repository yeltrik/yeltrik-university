<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dean
 * @property int id
 * @property int college_id
 * @property int wku_identity_id
 * @property College college
 * @property WkuIdentity wkuIdentity
 * @package App
 */
class Dean extends Model
{

    protected $connection = 'university';
    public $table = 'deans';

    CONST PROJECT_GID = '1190635546619881';

    /**
     * Dean constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function college()
    {
        return $this->belongsTo(College::class);
    }

    public function getTitleAttribute()
    {
        if ($this->wkuIdentity()->exists() ) {
            return $this->wkuIdentity->title;
        } else {
            return 'Anonymous';
        }
    }

    public function wkuIdentity()
    {
        return $this->belongsTo(WkuIdentity::class);
    }

}
