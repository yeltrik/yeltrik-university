<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FacultyRank
 * @property int id
 * @property string title
 * @package App
 */
class FacultyRank extends Model
{

    protected $connection = 'university';
    public $table = 'faculty_ranks';

    /**
     * FacultyRank constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function faculties()
    {
        return $this->hasMany(Faculty::class);
    }

}
