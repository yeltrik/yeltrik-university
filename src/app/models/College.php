<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;
use Yeltrik\Consultation\app\Consultation;
use Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster;

/**
 * Class College
 * @property int id
 * @property string name
 *
 * @property Faculty faculty
 * @package App
 */
class College extends Model
{

    protected $connection = 'university';
    public $table = 'colleges';

    /**
     * College constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function consultations()
    {
        $collegeId = $this->id;
        return Consultation::query()
            ->whereHas('wkuIdentity', function($query) use ($collegeId) {
                $query->where('college_id', '=', $collegeId);
            });
    }

    public function deans()
    {
        return $this->hasMany(Dean::class);
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    public function departmentHeads()
    {
        return $this->hasManyThrough(DepartmentHead::class, Department::class);
    }

    public function departmentsWithPdRoster(string $professionalDevelopmentRosterAttendedValue = NULL)
    {
//        $departmentIds = $this->professionalDevelopmentRosters()
//            ->select([
//                'wi.department_id as wi_department_id'
//            ])
//            ->where('attended', '=', $professionalDevelopmentRosterAttendedValue)
//            ->groupBy('wi_department_id');

        // TODO: We Loose "null/unknown" if ID has a Null Value

        return Department::query()
            ->whereHas('wkuIdentities', function($query) {
                $query->whereHas('professionalDevelopmentRosters', function($query) {

                });
            });
//            ->whereIn('departments.id', $departmentIds)
//            ->orderBy('departments.name', 'asc');
    }

    public function faculty()
    {
        $collegeId = $this->id;
        return Faculty::query()
            ->whereHas('wkuIdentity', function($query) use ($collegeId) {
                $query->where('college_id', '=', $collegeId);
            });
    }

    public function professionalDevelopmentRosters()
    {
        $collegeId = $this->id;
        return ProfessionalDevelopmentRoster::query()
            ->whereHas('wkuIdentity', function($query) use($collegeId) {
                $query->where('college_id', '=', $collegeId);
            });
    }

    public function professionalDevelopmentRostersWkuIdentityUnique()
    {
        return $this->professionalDevelopmentRosters();
    }

    public function wkuIdentities()
    {
        return $this->hasMany(WkuIdentity::class);
    }

//    public function staff()
//    {
//        $collegeId = $this->id;
//        return WkuStaff::query()
//            ->join('wku_identities as wi', function($join) use($collegeId){
//                $join->on('wi.id', '=', 'faculties.wku_identity_id');
//                $join->where('wi.college_id', '=', $collegeId);
//            });
//    }

}
