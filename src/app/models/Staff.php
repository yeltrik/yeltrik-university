<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{

    protected $connection = 'university';
    public $table = 'staff';

    /**
     * Staff constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function wkuIdentity()
    {
        return $this->belongsTo(WkuIdentity::class);
    }

}
