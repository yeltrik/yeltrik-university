<?php

namespace Yeltrik\University\app\models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Faculty
 * @property int id
 * @property int user_id
 * @property int wku_identity_id
 * @property int faculty_rank_id
 *
 * @property string title
 *
 * @property Department department
 * @property FacultyRank facultyRank
 * @property User user
 * @property WkuIdentity wkuIdentity
 * @package App
 */
class Faculty extends Model
{

    protected $connection = 'university';
    public $table = 'faculty';

    CONST PROJECT_GID = '1162922896446704';

    /**
     * Faculty constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function department()
    {
        if ( $this->wkuIdentity()->exists() ) {
            return $this->wkuIdentity->department();
        } else {
            return NULL;
        }
    }

    public function facultyRank()
    {
        return $this->belongsTo(FacultyRank::class);
    }

    public function getTitleAttribute()
    {
        $title = "Anonymous";
        if ( $this->user instanceof User ) {
            if ($this->user->name !== NULL) {
                $title = $this->user->name;
            }
            if ($this->user->email !== NULL) {
                $title = $this->user->email;
            }
        } elseif( $this->wkuIdentity instanceof WkuIdentity) {
            $title = $this->wkuIdentity->title;
        }

        return $title;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wkuIdentity()
    {
        return $this->belongsTo(WkuIdentity::class, 'wku_identity_id', 'id');
    }

}
