<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DepartmentHead
 * @property int id
 * @property int department_id
 * @property int wku_identity_id
 *
 * @property string email
 * @property string name
 *
 * @property Department department
 * @property WkuIdentity wkuIdentity
 *
 * @package App
 */
class DepartmentHead extends Model
{

    protected $connection = 'university';
    public $table = 'department_heads';

    CONST PROJECT_GID = '1181659344173641';

    /**
     * DepartmentHead constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function getEmailAttribute()
    {
        return $this->wkuIdentity->email;
    }

    public function getNameAttribute()
    {
        return $this->wkuIdentity->name;
    }

    public function getTitleAttribute()
    {
        return $this->wkuIdentity->title;
    }

    public function wkuIdentity()
    {
        return $this->belongsTo(WkuIdentity::class);
    }

}
