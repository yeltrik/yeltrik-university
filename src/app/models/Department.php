<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;
use Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster;

/**
 * Class Department
 * @property int id
 * @property int college_id
 * @property int name
 *
 * @property College college
 * @package App
 */
class Department extends Model
{

    protected $connection = 'university';
    public $table = 'departments';

    /**
     * Department constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function college()
    {
        return $this->belongsTo(College::class);
    }

    public function departmentHeads()
    {
        return $this->hasMany(DepartmentHead::class);
    }

    public function professionalDevelopmentRosters()
    {
        $departmentId = $this->id;
        return $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
            ->join('wku_identities as wi', function($join) use($departmentId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.department_id', '=', $departmentId);
            });
    }

    public function wkuIdentities()
    {
        return $this->hasMany(WkuIdentity::class);
    }

}
