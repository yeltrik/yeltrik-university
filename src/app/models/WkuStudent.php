<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;

class WkuStudent extends Model
{

    protected $connection = 'university';
    public $table = 'students';

    CONST PROJECT_GID = '1175422307058201';

    /**
     * WkuStudent constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

}
