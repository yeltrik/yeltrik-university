<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;

class WkuStaff extends Model
{

    protected $connection = 'university';
    public $table = 'staff';

    CONST PROJECT_GID = '1174901874472177';

    /**
     * WkuStaff constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

}
