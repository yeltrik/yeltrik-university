<?php

namespace Yeltrik\University\app\models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\Consultation\app\Consultation;
use Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster;
use Yeltrik\UniversityDepartment\app\CitlStaff;

/**
 * Class WkuIdentity
 * @property int id
 * @property int user_id
 * @property int college_id
 * @property int department_id
 * @property string name
 * @property string email
 * @property string netid
 * @property string wkuid
 * @property string asana_gid
 *
 * @property string title
 *
 * @property CitlStaff citlStaff
 * @property College college
 * @property Dean dean
 * @property Department department
 * @property DepartmentHead departmentHead
 * @property Faculty faculty
 * @property User user
 * @package App
 */
class WkuIdentity extends Model
{

    protected $connection = 'university';
    public $table = 'wku_identities';

    /**
     * WkuIdentity constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function asanaHref()
    {
        if ($this->isDean()) {
            return "https://app.asana.com/0/" . Dean::PROJECT_GID . "/" . $this->asana_gid . "/f";
        } elseif ($this->isDepartmentHead()) {
            return "https://app.asana.com/0/" . DepartmentHead::PROJECT_GID . "/" . $this->asana_gid . "/f";
        } elseif ($this->isFaculty()) {
            return "https://app.asana.com/0/" . Faculty::PROJECT_GID . "/" . $this->asana_gid . "/f";
        } elseif ($this->isCitlStaff()) {
            return "https://app.asana.com/0/" . CitlStaff::PROJECT_GID . "/" . $this->asana_gid . "/f";
        } else {
            // Search?
            return "#";
        }
    }

    public function attendedPd()
    {
        return (
        $this->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->exists()
        );
    }

    public function citlStaff()
    {
        return $this->hasOne(CitlStaff::class);
    }

    public function college()
    {
        return $this->belongsTo(College::class);
    }

    public function consultations()
    {
        return $this->hasMany(Consultation::class);
    }

    public function dean()
    {
        return $this->hasOne(Dean::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function departmentHead()
    {
        return $this->hasOne(DepartmentHead::class);
    }

    public function faculty()
    {
        return $this->hasOne(Faculty::class);
    }

    public function getTitleAttribute()
    {
        if ($this->name !== NULL) {
            return $this->name;
        }
        if ($this->email !== NULL) {
            return $this->email;
        }
        if ($this->netId !== NULL) {
            return $this->netId;
        }
        if ($this->wkuid !== NULL) {
            return $this->wkuid;
        }
        return "Anonymous";
    }

    public function isAsana()
    {
        return $this->asana_gid !== NULL;
    }

    public function isCitlStaff()
    {
        return $this->citlStaff()->exists();
    }

    public function isDean()
    {
        return $this->dean()->exists();
    }

    public function isDepartmentHead()
    {
        return $this->departmentHead()->exists();
    }

    public function isFaculty()
    {
        return $this->faculty()->exists();
    }

    public function professionalDevelopmentRosters()
    {
        return $this->hasMany(ProfessionalDevelopmentRoster::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
