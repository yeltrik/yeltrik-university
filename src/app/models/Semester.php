<?php

namespace Yeltrik\University\app\models;

use Illuminate\Database\Eloquent\Model;
use Yeltrik\Consultation\app\Consultation;
use Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentSession;

/**
 * Class Semester
 * @property int id
 * @property int year
 * @property string term
 *
 * @property string title
 * @package App
 */
class Semester extends Model
{

    protected $connection = 'university';
    public $table = 'semesters';

    public function __construct(array $attributes = [])
    {
        $this->table = env('DB_DATABASE_UNIVERSITY', $this->connection) . '.' . $this->table;
        parent::__construct($attributes);
    }

    public function consultations()
    {
        return $this->hasMany(Consultation::class);
    }

    public function professionalDevelopmentSessions()
    {
        return $this->hasMany(ProfessionalDevelopmentSession::class);
    }

    public function getTitleAttribute()
    {
        return $this->year . " " . $this->term;
    }

}
