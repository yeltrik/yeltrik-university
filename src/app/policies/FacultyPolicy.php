<?php

namespace Yeltrik\University\app\policies;

use Yeltrik\University\app\models\Faculty;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FacultyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Faculty $faculty
     * @return mixed
     */
    public function view(User $user, Faculty $faculty)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl() ||
            (
                $faculty->wkuIdentity->user instanceof User &&
                $user->id === $faculty->wkuIdentity->user->id
            )
        );
    }

}
