<?php

namespace Yeltrik\University\app\policies;

use App\Models\User;
use Yeltrik\University\app\models\WkuIdentity;
use Illuminate\Auth\Access\HandlesAuthorization;

class WkuIdentityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param WkuIdentity $wkuIdentity
     * @return mixed
     */
    public function view(User $user, WkuIdentity $wkuIdentity)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl() ||
            (
                $wkuIdentity->user()->exists() &&
                $user->id === $wkuIdentity->user->id
            )
        );
    }

}
