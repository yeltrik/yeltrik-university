<?php

namespace Yeltrik\University\app\policies;

use Yeltrik\University\app\models\Semester;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SemesterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Semester $semester
     * @return mixed
     */
    public function view(User $user, Semester $semester)
    {
        return (
          $user->isAdmin() ||
          $user->isCitl()
        );
    }

}
