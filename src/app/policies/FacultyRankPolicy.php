<?php

namespace Yeltrik\University\app\policies;

use Yeltrik\University\app\models\FacultyRank;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FacultyRankPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param FacultyRank $facultyRank
     * @return mixed
     */
    public function view(User $user, FacultyRank $facultyRank)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

}
