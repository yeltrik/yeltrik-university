<?php

namespace Yeltrik\University\app\policies;

use Yeltrik\University\app\models\DepartmentHead;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartmentHeadPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param DepartmentHead $departmentHead
     * @return mixed
     */
    public function view(User $user, DepartmentHead $departmentHead)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl() ||
            ( $departmentHead->wkuIdentity->user instanceof User && $user->id === $departmentHead->wkuIdentity->user->id ) ||
            (
                $user->isDean() &&
                $departmentHead->wkuIdentity->college->id === $user->wkuIdentity->college->id
            )
        );
    }

}
