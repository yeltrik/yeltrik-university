<?php

namespace Yeltrik\University\app\policies;

use Yeltrik\University\app\modelsDepartment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Department $department
     * @return mixed
     */
    public function view(User $user, Department $department)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl() ||
            (
                $user->isDean() &&
                $user->wkuIdentity()->exists() &&
                (int)$user->wkuIdentity->college_id === (int)$department->college_id
            ) ||
            (
                $user->isDepartmentHead() &&
                (
                    $user->department instanceof Department &&
                    $department->id === $user->department->id
                )
            )
        );
    }

}
