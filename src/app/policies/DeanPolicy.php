<?php

namespace Yeltrik\University\app\policies;

use Yeltrik\University\app\models\Dean;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Dean $dean
     * @return mixed
     */
    public function view(User $user, Dean $dean)
    {

        return (
            $user->isAdmin() ||
            $user->isCitl() ||
            (
                $dean->wkuIdentity->user instanceof User &&
                $user->id === $dean->wkuIdentity->user->id
            )
        );
    }

}
