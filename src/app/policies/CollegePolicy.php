<?php

namespace Yeltrik\University\app\policies;

use Yeltrik\University\app\models\College;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollegePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl()
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param College $college
     * @return mixed
     */
    public function view(User $user, College $college)
    {
        return (
            $user->isAdmin() ||
            $user->isCitl() ||
            (
                $user->isDean() &&
                $user->wkuIdentity()->exists() &&
                (int)$user->wkuIdentity->college_id === (int)$college->id
            )
        );
    }

}
