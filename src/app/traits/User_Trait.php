<?php

namespace Yeltrik\University\app\traits;

use Yeltrik\University\app\models\Faculty;
use Yeltrik\University\app\models\WkuIdentity;

trait User_Trait
{

    public function college()
    {
        return $this->wkuIdentity->college();
    }

    public function dean()
    {
        return $this->wkuIdentity->dean();
    }

    public function department()
    {
        return $this->wkuIdentity->department();
    }

    public function faculty()
    {
        return $this->hasOne(Faculty::class);
    }

    public function isDean()
    {
        if ( $this->wkuIdentity()->exists() ) {
            return (
            $this->wkuIdentity->isDean()
            );
        } else {
            return FALSE;
        }
    }

    public function isDepartmentHead()
    {
        if ( $this->wkuIdentity()->exists() ) {
            return (
            $this->wkuidentity->departmentHead()->exists()
            );
        } else {
            return FALSE;
        }
    }

    public function isFaculty()
    {
        return $this->faculty()->exists();
    }

    public function wkuIdentity()
    {
        return $this->hasOne(WkuIdentity::class);
    }
}
