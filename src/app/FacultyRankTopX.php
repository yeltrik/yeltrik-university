<?php


namespace Yeltrik\University\app;


class FacultyRankTopX
{

//    public function getAllData(): array
//    {
//        $allData = [
//            'fas' => 93,
//            'fad' => 42,
//            'gzs' => 5,
//            'rew' => 1,
//            'abc' => 150,
//            'asd' => 125,
//            'hgr' => 77,
//            'sdg' => 55,
//            'hdf' => 23,
//            'fds' => 8,
//        ];
//
//        return $allData;
//    }

    public function getTopXData(array $allData, bool $other = TRUE, int $otherLimit = 0, string $otherKey = 'Other'): array
    {
        if ($other === FALSE || empty($allData)) {
            return $allData;
        } else {
            if ($otherLimit > 0) {
                return $this->getTopXDataWithOtherLimit($allData, $otherLimit, $otherKey);
            } else {
                return $this->getTopXDataWithDynamicLimit($allData, $otherKey);
            }
        }
    }

    protected function getTopXDataWithDynamicLimit(array $allData, string $otherKey): array
    {
        arsort($allData);

        $keys = array_keys($allData);

        for ($i = 1; $i <= sizeof($keys); $i++) {
            $left = array_slice($allData, 0, $i);
            $right = array_slice($allData, $i);
            $lastLeftValue = array_pop($left);
            $sumRight = 0;
            foreach( $right as $value ) {
                $sumRight += $value;
            }
            if ( $lastLeftValue > $sumRight ) {
                $topXData = array_slice($allData, 0, $i);
                $topXData[$otherKey] = $sumRight;
                return $topXData;
            }
        }

        return $allData;
    }

    protected function getTopXDataWithOtherLimit(array $allData, int $otherLimit, string $otherKey): array
    {
        if ($otherLimit < sizeof($allData)) {
            arsort($allData);
            $topXData = array_slice($allData, 0, $otherLimit);
            $otherData = array_slice($allData, $otherLimit);
            $topXData[$otherKey] = 0;
            foreach ($otherData as $value) {
                $topXData[$otherKey] += $value;
            }
            return $topXData;
        } else {
            return $allData;
        }
    }

}
