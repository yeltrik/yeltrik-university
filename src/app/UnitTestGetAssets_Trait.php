<?php


namespace Yeltrik\University\app;


use App\Models\User;
use Yeltrik\University\app\models\College;
use Yeltrik\University\app\models\Dean;
use Yeltrik\University\app\models\Department;
use Yeltrik\University\app\models\DepartmentHead;
use Yeltrik\University\app\models\Faculty;
use Yeltrik\University\app\models\FacultyRank;
use Yeltrik\University\app\models\Semester;
use Yeltrik\University\app\models\WkuIdentity;

trait UnitTestGetAssets_Trait
{

    public function getAdminUser()
    {
        return User::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getCollege()
    {
        return College::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getDean()
    {
        return Dean::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getDepartment()
    {
        return Department::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getDepartmentHead()
    {
        return DepartmentHead::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getFaculty()
    {
        return Faculty::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getFacultyRank()
    {
        return FacultyRank::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getSemester()
    {
        return Semester::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

    public function getWkuIdentity()
    {
        return WkuIdentity::query()
            ->inRandomOrder()
            ->firstOrFail();
    }

}
