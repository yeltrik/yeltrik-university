<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\University\app\models\Department;
use Yeltrik\University\app\models\Faculty;
use Yeltrik\University\app\models\FacultyRank;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DepartmentFacultyRankController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;

        $departmentId = $department->id;
        $facultyRanks = FacultyRank::query()
            ->whereHas('faculties', function ($query) use ($departmentId) {
                $query->whereHas('wkuIdentity', function ($query) use ($departmentId) {
                    $query->where('department_id', '=', $departmentId);
                });
            })
            ->paginate(50);

        return view('university::department.faculty-rank.index', compact(
            'department',
            'college',
            'facultyRanks'
        ));
    }

    /**
     * @param Department $department
     * @param FacultyRank $facultyRank
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(Department $department, FacultyRank $facultyRank)
    {
        $this->authorize('view', $department);
        //$this->authorize('view', $facultyRank);

        $college = $department->college;

        $facultyRankId = $facultyRank->id;
        $departmentId = $department->id;
        $faculties = Faculty::query()
            ->where('faculty_rank_id', '=', $facultyRankId)
            ->whereHas('wkuIdentity', function($query) use($departmentId) {
                $query->where('department_id', '=', $departmentId);
            })
            ->paginate(50);

        return view('university::department.faculty-rank.show', compact(
            'department',
            'college',
            'facultyRank',
            'faculties'
        ));
    }

}
