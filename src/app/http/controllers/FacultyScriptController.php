<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Abstract_AsanaScriptController;
use Yeltrik\University\app\models\Faculty;

class FacultyScriptController extends Abstract_AsanaScriptController
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    static function getProjectGid(): string
    {
        return Faculty::PROJECT_GID;
    }

}
