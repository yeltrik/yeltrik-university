<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\University\app\models\Department;
use Yeltrik\University\app\models\Faculty;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class DepartmentFacultyController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;

        $departmentId = $department->id;
        $faculties = Faculty::query()
            ->whereHas('wkuIdentity', function ($query) use ($departmentId) {
                $query->where('department_id', '=', $departmentId);
            })
            ->paginate(50);

        return view('university::department.faculty.index', compact(
            'department',
            'college',
            'faculties'
        ));
    }

//    /**
//     * @param Department $department
//     * @param Faculty $faculty
//     * @return Application|Factory|View
//     * @throws AuthorizationException
//     */
//    public function show(Department $department, Faculty $faculty)
//    {
//        $this->authorize('view', $department);
//        $this->authorize('view', $faculty);
//
//        $college = $department->college;
//
//        return view('university::department.faculty.show', compact(
//            'department',
//            'college',
//            'faculty'
//        ));
//    }

}
