<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Yeltrik\University\app\models\College;

class CollegeDepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(College $college)
    {
        $this->authorize('view', $college);

        $departments = $college->departments()
            ->orderBy('name', 'asc')
            ->paginate(15);

        return view('university::college.department.index', compact('college', 'departments'));
    }

//    public function show(College $college)
//    {
//        //$this->authorize('view', $collegeName);
//
//        //dd($collegeName);
//
//        $collegeName = $college->name;
//        // custom_field_task, where there is a college / department on the same task
//        $query = DB::table('tasks')
//            ->select([
//                'eoCollege.name as college_name',
//                'eoDepartment.name as department_name',
//                DB::raw('count(eoCollege.name) as c'),
//            ])
//            ->join('custom_field_task as cftCollege', function ($join) {
//                $join->on('cftCollege.task_id', '=', 'tasks.id');
//            })
//            ->join('custom_fields as cfCollege', function ($join) {
//                $join->on('cfCollege.id', '=', 'cftCollege.custom_field_id');
//                $join->where('cfCollege.name', '=', 'College');
//            })
//            ->join('enum_options as eoCollege', function ($join) use ($collegeName) {
//                $join->on('eoCollege.id', '=', 'cftCollege.enum_value');
//                $join->where('eoCollege.name', '=', $collegeName);
//            })
//            ->join('custom_field_task as cftDepartment', function ($join) {
//                $join->on('cftDepartment.task_id', '=', 'tasks.id');
//            })
//            ->join('custom_fields as cfDepartment', function ($join) {
//                $join->on('cfDepartment.id', '=', 'cftDepartment.custom_field_id');
//                $join->where('cfDepartment.name', '=', 'Department');
//            })
//            ->join('enum_options as eoDepartment', function ($join) {
//                $join->on('eoDepartment.id', '=', 'cftDepartment.enum_value');
//            })
//            ->groupBy([
//                'eoCollege.name',
//                'eoDepartment.name',
//            ])
//            ->get();
//
//        //dd($query);
//
//        $departments = [];
//
//        foreach ($query as $item) {
//            $departments[] = $item->department_name;
//        }
//
//        ksort($departments);
//
//        //dd($departments);
//
//        return view('university::college.show', compact('college', 'departments'));
//    }

}
