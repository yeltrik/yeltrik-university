<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\ProfessionalDevelopment\app\http\controllers\CollegeAttendanceController;
use Yeltrik\ProfessionalDevelopment\app\http\controllers\DepartmentAttendanceController;
use Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster;
use Yeltrik\University\app\models\College;

class CollegeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', College::class);

        $colleges = College::query()
            ->orderBy('name', 'asc')
            ->paginate(15);

        $collegeAttendanceController = new CollegeAttendanceController();
        $collegeAttendanceData = $collegeAttendanceController
            ->chartJsData(ProfessionalDevelopmentRoster::all());

        return view('university::college.index', compact(
            'colleges',
            'collegeAttendanceData'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param College $college
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(College $college)
    {
        $this->authorize('view', $college);

        $colleges = College::query()
        ->orderBy('name', 'asc')
        ->paginate(15);

        $collegeId = $college->id;
        $professionalDevelopmentRosters = $college->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->get();

        $departmentAttendanceController = new DepartmentAttendanceController();
        $departmentAttendanceData = $departmentAttendanceController
            ->chartJsData($professionalDevelopmentRosters);

        return view('university::college.show', compact(
            'college', 'colleges',
            'departmentAttendanceData'
        ));
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function departmentHead(College $college)
    {
        $this->authorize('view', $college);

        $departmentHeads = $college->departmentHeads()
            ->join('wku_identities as wi', function($join){
                $join->on('wi.id', '=', 'department_heads.wku_identity_id');
            })
            ->orderBy('wi.name', 'asc')
            ->paginate(15);

        return view('university::college.dept-head', compact('college', 'departmentHeads'));
    }

}
