<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\University\app\models\Department;
use Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class DepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Department::class);

        $departments = Department::query()
            ->orderBy('name', 'asc')
            ->paginate(10);

        return view('university::department.index', compact('departments'));
    }

    /**
     * Display the specified resource.
     *
     * @param Department $department
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;
        $departmentHeads = $department->departmentHeads;

        $professionalDevelopmentRosters = (new ProfessionalDevelopmentRoster())
            ->attendanceForDepartment($department, ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->paginate(10);

        return view('university::department.show', compact(
            'department', 'college',
            'departmentHeads',
            'professionalDevelopmentRosters'
        ));
    }

}
