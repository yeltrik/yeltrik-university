<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\University\app\models\Dean;

class DeanController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Dean::class);

        $deans = Dean::query()
            ->select([
                'deans.*',
            ])
            ->join('colleges as c', function($join){
                $join->on('c.id', '=', 'deans.college_id');
            })
            ->orderBy('c.name', 'asc')
            ->paginate(10);

        return view('university::dean.index', compact('deans'));
    }

    /**
     * Display the specified resource.
     *
     * @param Dean $dean
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(Dean $dean)
    {
        $this->authorize('view', $dean);

        return view('university::dean.show', compact('dean'));
    }

}
