<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\University\app\models\College;
use Yeltrik\University\app\models\FacultyRank;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class CollegeFacultyRankController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(College $college)
    {
        $this->authorize('view', $college);

        $facultyRanks = static::facultyRanks($college)
            ->paginate(50);

        return view('university::college.faculty-rank.index', compact('college', 'facultyRanks'));
    }

    public function show(College $college, FacultyRank $facultyRank)
    {
        $this->authorize('view', $college);
        //$this->authorize('view', $facultyRank);

        $facultyRanks = static::facultyRanks($college)
            ->paginate(50);

        return view('university::college.faculty-rank.show', compact(
            'college',
            'facultyRank', 'facultyRanks'
        ));
    }

    public static function facultyRanks(College $college)
    {
        return FacultyRank::query()
            ->with('faculties', function($query) {
                $query->with('wkuIdentity');
            })
            ->orderBy('title', 'asc');
    }

}
