<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\University\app\models\DepartmentHead;

class DepartmentHeadController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', DepartmentHead::class);

        $departmentHeads = DepartmentHead::query()
            ->paginate(10);

        return view('university::dept-head.index', compact('departmentHeads'));
    }

    /**
     * Display the specified resource.
     *
     * @param DepartmentHead $departmentHead
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(DepartmentHead $departmentHead)
    {
        $this->authorize('view', $departmentHead);

        return view('university::dept-head.show', compact('departmentHead'));
    }

}
