<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Yeltrik\University\app\models\Course;

class CourseSemesterController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Course::class);

        // TODO in Courses Project

        $query = DB::table('tasks')
            ->select([
                'enum_options.name as semester_name',
                DB::raw('count(enum_options.name) as c'),
            ])
            ->join('project_task', function ($join) {
                $join
                    ->on('tasks.id', '=', 'project_task.task_id')
                    ->where('project_task.project_id', '=', Course::PROJECT_GID);
            })
            ->join('custom_field_task', function($join) {
                $join->on('custom_field_task.task_id', '=', 'tasks.id');
            })
            ->join('custom_fields', function($join){
                $join->on('custom_fields.id', '=', 'custom_field_task.custom_field_id');
                $join->where('custom_fields.name', '=', 'Semester');
            })
            ->join('enum_options', function ($join) {
                $join->on('enum_options.id', '=', 'custom_field_task.enum_value');
            })
            ->groupBy([
                'semester_name'
            ])
            ->get();

        //dd($query);

        $semesterCounts = [];
        foreach($query as $item) {
            $semesterCounts[$item->semester_name] = $item->c;
        }

        ksort($semesterCounts);

        return view('university::course.semester.index', compact('semesterCounts'));
    }

    /**
     * Display the specified resource.
     *
     * @param string $semester
     * @return Application|Factory|Response|View
     */
    public function show(string $semester)
    {
        // TODO: How do we Authorize??  Need a Policy for This controller, and a Model??
        //$this->authorize('view', Course::class);

        if ($semester === 'null') {
            $semesterWhere = null;
        } else {
            $semesterWhere = $semester;
        }

        $query = DB::table('tasks')
            ->select([
                'tasks.id as task_id',
            ])
            ->join('custom_field_task', function($join) {
                $join->on('custom_field_task.task_id', '=', 'tasks.id');
            })
            ->join('custom_fields', function($join){
                $join->on('custom_fields.id', '=', 'custom_field_task.custom_field_id');
                $join->where('custom_fields.name', '=', 'Semester');
            })
            ->join('enum_options', function ($join) use($semesterWhere) {
                $join->on('enum_options.id', '=', 'custom_field_task.enum_value');
                $join->where('enum_options.name', '=', $semesterWhere);
            })
            ->distinct();
//            ->get();

//        dd($query);

        $courses = Course::whereIn('id', $query)
            ->paginate(15);

        //dd($courses);

        return view('university::course.semester.show', compact('semester', 'courses'));
    }

}
