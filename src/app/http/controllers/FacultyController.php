<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\University\app\models\Faculty;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yeltrik\University\app\models\FacultyRank;
use Yeltrik\University\app\models\WkuIdentity;

class FacultyController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Faculty::class);

        $faculties = Faculty::query()
//            ->select([
//                'fr.id as faculty_rank_id',
//                'fr.title as faculty_rank_title',
//            ])
            ->whereHas('wkuIdentity')
            ->whereHas('facultyRank')
//            ->orderBy('wi.name', 'asc')
            ->paginate(10);

        return view('university::faculty.index', compact('faculties'));
    }

    /**
     * Display the specified resource.
     *
     * @param Faculty $faculty
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(Faculty $faculty)
    {
        $this->authorize('view', $faculty);

        return view('university::faculty.show', compact('faculty'));
    }

}
