<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\University\app\models\Faculty;
use Yeltrik\University\app\models\FacultyRank;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class FacultyRankController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', FacultyRank::class);

        $facultyRanks = FacultyRank::query()
            ->orderBy('title', 'asc')
            ->paginate(15);

        return view('university::faculty-rank.index', compact('facultyRanks'));
    }

    /**
     * Display the specified resource.
     *
     * @param FacultyRank $facultyRank
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(FacultyRank $facultyRank)
    {
        $this->authorize('view', $facultyRank);

        $facultyRankId = $facultyRank->id;
        $faculties = Faculty::query()
            ->with('facultyRank', function($query) use($facultyRankId) {
                $query->where('id', '=', $facultyRankId);
            })
            ->paginate(50);

        return view('university::faculty-rank.show', compact('facultyRank', 'faculties'));
    }

}
