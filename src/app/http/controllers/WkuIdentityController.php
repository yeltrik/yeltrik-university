<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster;
use Yeltrik\University\app\models\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class WkuIdentityController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', WkuIdentity::class);

        $countWithEmail = WkuIdentity::query()->whereNotNull('email')->count();
        $countWithNetId = WkuIdentity::query()->whereNotNull('netid')->count();
        $countWithWkuId = WkuIdentity::query()->whereNotNull('wkuid')->count();
        $countWithUserId = WkuIdentity::query()->whereNotNull('user_id')->count();
        $countWithAsana = WkuIdentity::query()->whereNotNull('asana_gid')->count();
        $countWithCollege = WkuIdentity::query()->whereNotNull('college_id')->count();
        $countWithDepartment = WkuIdentity::query()->whereNotNull('department_id')->count();

        $wkuIdentities = WkuIdentity::query()
            ->orderBy('name', 'asc')
            ->orderBy('email', 'asc')
            ->orderBy('netid', 'asc')
            ->paginate(10);

        return view('university::wku-identity.index', compact(
            'wkuIdentities',
            'countWithEmail',
            'countWithNetId',
            'countWithWkuId',
            'countWithUserId',
            'countWithAsana',
            'countWithCollege',
            'countWithDepartment'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param WkuIdentity $wkuIdentity
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(WkuIdentity $wkuIdentity)
    {
        $this->authorize('view', $wkuIdentity);

        $hasPdRoster = ProfessionalDevelopmentRoster::query()
            ->where('wku_identity_id', '=', $wkuIdentity->id)
            ->exists();

        return view('university::wku-identity.show', compact('wkuIdentity', 'hasPdRoster'));
    }

}
