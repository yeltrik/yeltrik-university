<?php

namespace Yeltrik\University\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\University\app\models\College;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class CollegeFacultyController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(College $college)
    {
        $this->authorize('view', $college);

        $faculties = $college->faculty()
            ->paginate(50);

        return view('university::college.faculty.index', compact('college', 'faculties'));
    }

}
