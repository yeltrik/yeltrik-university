<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\DepartmentHeadController;

Route::get('dept-head/',
    [DepartmentHeadController::class, 'index'])
    ->name('dept-heads.index');

Route::get('dept-head/{departmentHead}',
    [DepartmentHeadController::class, 'show'])
    ->name('dept-heads.show');

