<?php


use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\WkuIdentityController;
use Yeltrik\University\app\http\controllers\PdWkuIdentityRosterController;

Route::get('wku-identity',
    [WkuIdentityController::class, 'index'])
    ->name('wku-identities.index');

Route::get('wku-identity/{wkuIdentity}',
    [WkuIdentityController::class, 'show'])
    ->name('wku-identities.show');

