<?php


use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\DeanController;

Route::get('dean', [DeanController::class, 'index'])->name('deans.index');
Route::get('dean/{dean}', [DeanController::class, 'show'])->name('deans.show');
