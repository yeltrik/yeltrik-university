<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\CollegeFacultyController;


require 'rank/web.php';

Route::get('college/{college}/faculty',
    [CollegeFacultyController::class, 'index'])
    ->name('colleges.faculty.index');
