<?php

use Yeltrik\University\app\http\controllers\CollegeFacultyRankController;
use Illuminate\Support\Facades\Route;


Route::get('college/{college}/faculty/rank',
    [CollegeFacultyRankController::class, 'index'])
    ->name('colleges.faculty-ranks.index');

Route::get('college/{college}/faculty/rank/{facultyRank}',
    [CollegeFacultyRankController::class, 'show'])
    ->name('colleges.faculty-ranks.show');
