<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\CollegeDepartmentController;

Route::get('college/{college}/department',
    [CollegeDepartmentController::class, 'index'])
    ->name('colleges.departments.index');
