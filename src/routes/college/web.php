<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\CollegeController;

Route::get('college',
    [CollegeController::class, 'index'])
    ->name('colleges.index');

Route::get('college/{college}',
    [CollegeController::class, 'show'])
    ->name('colleges.show');

require 'department/web.php';
require 'dept-head/web.php';
require 'faculty/web.php';
