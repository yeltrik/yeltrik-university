<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\CollegeController;


Route::get('college/{college}/dept-head',
    [CollegeController::class, 'departmentHead'])
    ->name('colleges.department-head');
