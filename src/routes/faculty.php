<?php


use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\FacultyController;

Route::get('faculty',
    [FacultyController::class, 'index'])
    ->name('faculty.index');

Route::get('faculty/{faculty}',
    [FacultyController::class, 'show'])
    ->name('faculty.show');
