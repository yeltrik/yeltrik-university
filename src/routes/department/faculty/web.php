<?php

use Yeltrik\University\app\http\controllers\DepartmentFacultyController;
use Illuminate\Support\Facades\Route;

require 'rank/web.php';

Route::get('department/{department}/faculty',
    [DepartmentFacultyController::class, 'index'])
    ->name('departments.faculty.index');
