<?php

use Yeltrik\University\app\http\controllers\DepartmentFacultyRankController;
use Illuminate\Support\Facades\Route;


Route::get('department/{department}/faculty/rank',
    [DepartmentFacultyRankController::class, 'index'])
    ->name('departments.faculty-ranks.index');

Route::get('department/{department}/faculty/rank/{facultyRank}',
    [DepartmentFacultyRankController::class, 'show'])
    ->name('departments.faculty-ranks.show');
