<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\DepartmentController;

Route::get('department', [DepartmentController::class, 'index'])->name('departments.index');
Route::get('department/{department}', [DepartmentController::class, 'show'])->name('departments.show');

require 'faculty/web.php';
