<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\FacultyRankController;

Route::get('faculty-rank',
    [FacultyRankController::class, 'index'])
    ->name('faculty-ranks.index');

Route::get('faculty-rank/{facultyRank}',
    [FacultyRankController::class, 'show'])
    ->name('faculty-ranks.show');
