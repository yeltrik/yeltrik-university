<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\University\app\http\controllers\CourseController;
use Yeltrik\University\app\http\controllers\CourseSemesterController;

require 'college/web.php';
require 'dean.php';
require 'department/web.php';
require 'dept-head.php';
require 'faculty.php';
require 'faculty-rank.php';
require 'wku-identity.php';
