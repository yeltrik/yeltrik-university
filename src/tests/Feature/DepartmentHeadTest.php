<?php

namespace Yeltrik\University\tests\Feature;

use App\UnitTestGetAssets_Trait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DepartmentHeadTest extends TestCase
{

    //use UnitTestGetAssets_Trait;
    use \Yeltrik\University\app\UnitTestGetAssets_Trait;
    //use \Yeltrik\UniversityDepartment\app\UnitTestGetAssets_Trait;

    public function testRouteIndex()
    {
        $user = $this->getAdminUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('dept-heads.index'));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getAdminUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('dept-heads.show',
            $this->getDepartmentHead()));
        $response->assertStatus(200);
    }

}
