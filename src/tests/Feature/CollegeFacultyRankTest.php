<?php

namespace Yeltrik\University\tests\Feature;

use App\UnitTestGetAssets_Trait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CollegeFacultyRankTest extends TestCase
{

    //use UnitTestGetAssets_Trait;
    use \Yeltrik\University\app\UnitTestGetAssets_Trait;
    //use \Yeltrik\UniversityDepartment\app\UnitTestGetAssets_Trait;

    public function testRouteIndex()
    {
        $user = $this->getAdminUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('colleges.faculty-ranks.index',
                $this->getCollege()));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getAdminUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('colleges.faculty-ranks.show',
                [$this->getCollege(), $this->getFacultyRank() ]));
        $response->assertStatus(200);
    }
}
