<?php

namespace Yeltrik\University\tests\Feature;

use App\UnitTestGetAssets_Trait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CollegeDepartmentTest extends TestCase
{

    //use UnitTestGetAssets_Trait;
    use \Yeltrik\University\app\UnitTestGetAssets_Trait;
    //use \Yeltrik\UniversityDepartment\app\UnitTestGetAssets_Trait;


    public function testRouteIndex()
    {
        $user = $this->getAdminUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('colleges.departments.index',
            $this->getCollege()));
        $response->assertStatus(200);
    }

//    public function testRouteShow()
//    {
//        $user = $this->getAdminUser();
//        $response = $this->actingAs($user, 'web')
//            ->get(route('colleges.departments.show',
//                [$this->getCollege(),$this->getDepartment()]));
//        $response->assertStatus(200);
//    }

}
