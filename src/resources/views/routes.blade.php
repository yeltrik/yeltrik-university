<div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
    <a href="{{route('colleges.index')}}">Colleges</a>
    <a href="{{route('deans.index')}}">Deans</a>
    <a href="{{route('departments.index')}}">Department</a>
    <a href="{{route('dept-heads.index')}}">Department Heads</a>
    <a href="{{route('faculty.index')}}">Faculty</a>
    <a href="{{route('wku-identities.index')}}">WKU Identities</a>
</div>
