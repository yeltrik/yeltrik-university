@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Deans</li>
            </ol>
        </nav>

        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col" class="text-center">College</th>
                <td class="text-center">Dean</td>
            </tr>
            </thead>
            <tbody>

            @foreach($deans as $dean)
                <tr>
                    <td>
                        @if( $dean->college instanceof \Yeltrik\University\app\College )
                        <a
                            class="btn btn-outline-light btn-block"
                            href="{{ route('colleges.show', $dean->college) }}"
                        >
                            {{ $dean->college->name }}
                        </a>
                        @endif
                    </td>
                    <th scope="row">
                        <a
                            class="btn btn-light btn-block text-dark"
                            href="{{ route('deans.show', $dean) }}"
                        >
                            {{ $dean->wkuIdentity->name }}
                        </a>
                    </th>
                </tr>
            @endforeach

            </tbody>
        </table>

        {{ $deans->links() }}

    </div>
@endsection
