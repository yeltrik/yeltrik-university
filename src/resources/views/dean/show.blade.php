@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @can('viewAny', Yeltrik\University\app\Dean::class)
                    <li class="breadcrumb-item">
                        <a href="{{ route('deans.index') }}">
                            Deans
                        </a>
                    </li>
                @endcan
                <li class="breadcrumb-item active" aria-current="page">{{ $dean->wkuIdentity->name }}</li>
            </ol>
        </nav>

        <div class="d-flex justify-content-between">
            <div>
                <h1>
                    @can('view', $dean)
                        @include('university::link.dean', ['dean' => $dean])
                    @endcan
                    {{ $dean->title }}
                </h1>
            </div>

            <div class="d-flex float-right">
                @include('university::dean.icons')
            </div>
        </div>


        @if( $dean->college instanceof \Yeltrik\University\app\College )
            <h3>
                Dean of
                @can('view', $dean->college)
                    <a
                        class="text-dark"
                        href="{{ route('colleges.show', $dean->college) }}"
                    >
                        {{ $dean->college->name }}
                    </a>
                @else
                    {{ $dean->college->name }}
                @endcan
            </h3>
        @endif

        @if( $dean->wkuIdentity->professionalDevelopmentRosters()->where('attended', '=', \Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster::ATTENDED_YES)->count() )
            PD Sessions
            Attended: {{ $dean->wkuIdentity->professionalDevelopmentRosters()->where('attended', '=', \Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster::ATTENDED_YES)->count() }}
        @endif

        {{--@include('pd.roster.table.session')--}}

    </div>
@endsection
