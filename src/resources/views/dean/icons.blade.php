@if($dean->wkuIdentity()->exists())

    @if($dean->wkuIdentity->isAsana())
        @can('view-asana')
            <div class="ml-2">
                @include('project::link.asana', ['wkuIdentity' => $dean->wkuIdentity])
            </div>
        @endcan
    @endif

    @if($dean->wkuIdentity->isDepartmentHead())
        @can('view', $dean->wkuIdentity->departmentHead)
            <div class="ml-2">
                @include('university::link.dept-head', ['departmentHead' => $dean->wkuIdentity->departmentHead])
            </div>
        @endcan
    @endif

    @if($dean->wkuIdentity->isCitlStaff())
        <div class="ml-2">
            @can('view', $dean->wkuIdentity->citlStaff)
                @include('universityDepartment::link.citl-staff', ['citlStaff' => $dean->wkuIdentity->citlStaff])
            @endcan
        </div>
    @endif

    @if($dean->wkuIdentity->isFaculty())
        <div class="ml-2">
            @can('view', $dean->wkuIdentity->faculty)
                @include('university::link.faculty', ['faculty' => $dean->wkuIdentity->faculty])
            @endcan
        </div>
    @endif

    @if($dean->wkuIdentity->user()->exists())
        @can('view', $dean->wkuIdentity->user)
            <div class="ml-2">
                @include('project::link.user', ['user' => $dean->wkuIdentity->user])
            </div>
        @endcan
    @endif

    @can('view', $dean->wkuIdentity)
        <div class="ml-2">
            @include('university::link.wku-identity', ['wkuIdentity' => $dean->wkuIdentity])
        </div>
    @endcan

@endif
