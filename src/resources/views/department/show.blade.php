@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')
    @include('university::college.breadcrumb.item.show')
    @include('university::department.breadcrumb.item.show')

@endsection

@section('reporting.title')

    @if($department->college instanceof \Yeltrik\University\app\College)
        <h3>
            @can('view', $department->college)
                <a
                    class="text-dark"
                    href="{{ route('colleges.show', $department->college) }}"
                >
                    {{ $department->college->name }}
                </a>
            @else
                {{ $department->college->name }}
            @endcan
        </h3>
    @endif

    <h1>
        {{ $department->name }}
    </h1>

@endsection

<?php $accordionId = 'pd-accordion' ?>
@section('reporting.accordion.card')

    @include('university::department.accordion.card.pivot-show')
{{--    @include('university::department.accordion.card.summary-show')--}}
{{--    @include('university::department.accordion.card.department-head')--}}

@endsection
