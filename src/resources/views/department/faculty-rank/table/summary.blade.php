<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center">Faculty Rank</th>
        <th scope="col" class="text-center">Count</th>
    </tr>
    </thead>
    <tbody>
    @foreach($facultyRanks as $facultyRank)
        <tr>
            <td class="text-center">
                <a
                    class="btn btn-outline-light btn-block"
                    href="{{ route('departments.faculty-ranks.show', [$department, $facultyRank]) }}"
                >
                    {{ $facultyRank->title }}
                </a>
            </td>
            <td class="text-center">
                {{ $facultyRank->faculties_count }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
