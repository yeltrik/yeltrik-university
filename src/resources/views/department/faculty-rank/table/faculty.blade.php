<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center">Faculty</th>
    </tr>
    </thead>
    <tbody>
    @foreach($faculties as $faculty)
        <tr>
            <td class="text-center">
                @can('view', $faculty)
                    <a
                        class="btn btn-outline-light btn-block"
                        href="{{ route('faculty.show', [$department, $faculty]) }}"
                    >
                        {{ $faculty->title }}
                    </a>
                @else
                    {{ $faculty->title }}
                @endcan
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $faculties->links() }}
