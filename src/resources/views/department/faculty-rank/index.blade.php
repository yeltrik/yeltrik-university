@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')
    @include('university::college.breadcrumb.item.show')
    @include('university::department.breadcrumb.item.show')
    @include('university::department.faculty.breadcrumb.item.show')
    @include('university::department.faculty-rank.breadcrumb.item.index')

@endsection

@section('reporting.title')

    {{ $college->name }} / {{ $department->name }}

@endsection

<?php $accordionId = 'department-faculty-rank-accordion' ?>
@section('reporting.accordion.card')

    @include('university::department.faculty-rank.accordion.card.pivot')
    @include('university::department.faculty-rank.accordion.card.faculty-rank')
{{--    @include('university::department.faculty-rank.accordion.card.summary')--}}

@endsection
