@extends('university::layouts.accordion.card.faculty-rank', ['id' => 'department'])

@section('accordion-card-faculty-rank-body-content')

    <p>
        The following departments have reportable PD participation for {{ $department->name }}.
    </p>

    <div class="row">
        <div class="col-4 mt-4">
            <a
                class="btn btn-outline-primary btn-block disabled"
                href="#"
            >
                Unknown
            </a>
        </div>
        @foreach($facultyRanks as $facultyRank)
            <div class="col-4 mt-4">
                <a
                    class="btn btn-outline-primary btn-block"
                    href="{{ route('departments.faculty-ranks.show', [$department, $facultyRank]) }}"
                >
                    {{ $facultyRank->title }}
                </a>
            </div>
        @endforeach
    </div>

@endsection
