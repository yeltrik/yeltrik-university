@extends('project::layouts.accordion.card.pivot', ['id' => 'department-faculty-rank'])

@section('accordion-card-pivot-body-content')

    @include('project::accordion.card.pivot.tip')

    <div class="row">

{{--        @include('pivot', ['text' => 'Faculty', 'route' => route('departments.faculty.index', $department)])--}}
        @include('project::pivot', ['text' => 'Professional Development', 'route' => route('pds.departments.faculty-ranks.index', $department)])

    </div>

@endsection
