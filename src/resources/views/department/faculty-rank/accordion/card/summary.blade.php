@extends('project::layouts.accordion.card.summary', ['id' => 'department-faculty-rank'])

@section('accordion-card-summary-body-content')

    @include('university::department.faculty-rank.table.summary')

@endsection
