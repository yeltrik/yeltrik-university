@extends('project::layouts.accordion.card.pivot', ['id' => 'department-faculty-rank'])

@section('accordion-card-pivot-body-content')

    @include('project::accordion.card.pivot.tip')

    <div class="row">

        @include('project::pivot', ['text' => 'Faculty', 'route' => route('departments.faculty.index', $department)])
{{--        @include('pivot', ['text' => 'Professional Development', 'route' => route('pds.departments.faculty-ranks.show', [$department, $facultyRank])])--}}

    </div>

@endsection
