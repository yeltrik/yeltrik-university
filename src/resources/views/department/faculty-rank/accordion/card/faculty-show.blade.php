@extends('university::layouts.accordion.card.faculty', ['id' => 'department.faculty-rank'])

@section('accordion-card-faculty-body-content')

    <p>
        Faculty for {{ $department->name }} with a rank of {{ $facultyRank->title }}.
    </p>

    @include('university::department.faculty-rank.table.faculty')

@endsection
