@can('viewAny', Yeltrik\University\app\Department::class)
    <li class="breadcrumb-item"><a href="{{ route('departments.index') }}">Department</a></li>
@endcan

@can('view', $department)
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('departments.show', $department) }}">
            {{ $department->name }}
        </a>
    </li>
@else
    <li class="breadcrumb-item active" aria-current="page">
        {{ $department->name }}
    </li>
@endcan
