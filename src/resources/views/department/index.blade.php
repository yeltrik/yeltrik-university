@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Departments</li>
            </ol>
        </nav>

        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">College</th>
                <th scope="col">Department</th>
            </tr>
            </thead>
            <tbody>

            @foreach($departments as $department)
                <tr>
                    <td>
                        @if($department->college instanceof \Yeltrik\University\app\College)
                            <a href="{{ route('colleges.show', $department->college) }}" class="text-dark">
                                {{ $department->college->name }}
                            </a>
                        @endif
                    </td>
                    <th scope="row">
                        <a href="{{ route('departments.show', $department) }}" class="text-dark">
                            {{ $department->name }}
                        </a>
                    </th>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $departments->links() }}

    </div>
@endsection
