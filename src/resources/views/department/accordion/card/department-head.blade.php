<?php $cardHeader = 'department-dept-head-summary-heading' ?>
<?php $cardCollapse = 'department-dept-head-summary-collapse' ?>
<div class="card">
    <div class="card-header" id="{{$cardHeader}}">
        <h2 class="mb-0">
            <button
                class="btn btn-link"
                type="button"
                data-toggle="collapse"
                data-target="#{{$cardCollapse}}"
                aria-expanded="false"
                aria-controls="{{$cardCollapse}}"
            >
                Department Heads
            </button>
        </h2>
    </div>

    <div
        id="{{$cardCollapse}}"
        class="collapse"
        aria-labelledby="{{$cardHeader}}"
        data-parent="#{{$accordionId}}"
    >
        <div class="card-body">

            @include('dept-head.list-group')

        </div>

    </div>
</div>
