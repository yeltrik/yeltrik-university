@extends('project::layouts.accordion.card.pivot', ['id' => 'department'])

@section('accordion-card-pivot-body-content')

    {{--@include('project::accordion.card.pivot.tip')--}}

    <p>
        Please use the buttons below to identify the manner in which you would like to view data from your department's CITL participation.
    </p>

    <div class="row">

        @include('project::pivot', ['text' => 'Faculty Rank', 'route' => route('departments.faculty-ranks.index', $department)])
        @include('project::pivot', ['text' => 'Professional Development', 'route' => route('pds.departments.faculty.index', $department)])

    </div>

@endsection
