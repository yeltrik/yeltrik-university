@extends('university::layouts.accordion.card.faculty', ['id' => 'department'])

@section('accordion-card-faculty-body-content')

    <p>
        The following is the faculty list which determines the reporting for {{ $department->name }}.
    </p>

    <table class="table table-hover table-dark">
        <thead>
        <tr>
            <th scope="col" class="text-center">Faculty</th>
            <th scope="col" class="text-center">Rank</th>
        </tr>
        </thead>
        <tbody>
        @foreach($faculties as $faculty)
            <tr>
                <th scope="row" class="text-center">
                    @can('view', $faculty)
                        <a
                            class="btn btn-outline-light btn-block"
                            href="{{ route('faculty.show', $faculty) }}"
                        >
                            {{ $faculty->title }}
                        </a>
                    @else
                        {{ $faculty->title }}
                    @endcan
                </th>
                <td class="text-center">
                    @if( $faculty->facultyRank()->exists() )
                        {{--@can('view', $faculty->facultyRank)--}}
                        <a
                            class="btn btn-outline-light btn-block"
                            href="{{ route('departments.faculty-ranks.show', [$department, $faculty->facultyRank]) }}"
                        >
                            {{ $faculty->facultyRank->title }}
                        </a>
                        {{--@else--}}
                        {{--    {{ $faculty->facultyRank->title }}--}}
                        {{--@endcan--}}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $faculties->links() }}

    <a
        class="btn btn-outline-warning btn-block text-dark"
        href="mailto:steven.kirtley@wku.edu"
    >
        If you notice something that needs to be corrected, please contact steven.kirtley@wku.edu.
    </a>

@endsection
