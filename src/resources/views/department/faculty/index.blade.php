@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')
    @include('university::college.breadcrumb.item.show')
    @include('university::department.breadcrumb.item.show')
    @include('university::department.faculty.breadcrumb.item.index')

@endsection

@section('reporting.title')

    {{ $department->name }}

@endsection

<?php $accordionId = 'department-faculty-accordion' ?>
@section('reporting.accordion.card')

    @include('university::department.faculty.accordion.card.pivot')
    @include('university::department.faculty.accordion.card.faculty')

@endsection
