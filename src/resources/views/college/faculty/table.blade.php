<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center">Faculty</th>
        <th scope="col" class="text-center">Department</th>
        <th scope="col" class="text-center">Rank</th>
    </tr>
    </thead>
    <tbody>

    @foreach($faculties as $faculty)
        <tr>
            <td class="text-center">
                @can('view', $faculty)
                    <a
                        class="btn btn-outline-light btn-block"
                        href="{{ route('faculty.show', $faculty) }}"
                    >
                        {{ $faculty->name }}
                    </a>
                @else
                    {{ $faculty->name }}
                @endcan
            </td>
            <td>
                @if($faculty->department()->exists())
                    <a
                        class="btn btn-outline-light btn-block"
                        href="{{ route('departments.faculty.index', $faculty->department) }}"
                    >
                        {{ $faculty->department->name }}
                    </a>
                @endif
            </td>
            <td class="text-center">
                @if($faculty->facultyRank()->exists())
                    <a
                        class="btn btn-outline-light btn-block"
                        href="{{ route('colleges.faculty-ranks.show', [$college, $faculty->facultyRank]) }}"
                    >
                        {{ $faculty->facultyRank->title }}
                    </a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $faculties->links() }}
