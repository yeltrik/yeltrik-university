@extends('project::layouts.accordion.card.pivot', ['id' => 'college'])

@section('accordion-card-pivot-body-content')

    {{--@include('project::accordion.card.pivot.tip')--}}
    <p>
        Please use the buttons below to identify the manner in which you would like to view data from your department's CITL participation.
    </p>

    <div class="row">

        @include('project::pivot', ['text' => 'Department', 'route' => route('colleges.departments.index', $college)])
        @include('project::pivot', ['text' => 'Faculty Ranks', 'route' => route('colleges.faculty-ranks.index', $college)])
        @include('project::pivot', ['text' => 'Professional Development', 'route' => route('pds.colleges.show', $college)])

    </div>

@endsection
