@extends('university::layouts.accordion.card.faculty', ['id' => 'department'])

@section('accordion-card-faculty-body-content')

    <p>
        Faculty for {{ $college->name }}.
    </p>

    @include('university::college.faculty.table')

@endsection
