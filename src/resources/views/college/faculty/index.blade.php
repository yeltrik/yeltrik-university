@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')

    @can('viewAny', \Yeltrik\University\app\College::class)
        <li class="breadcrumb-item"><a href="{{ route('colleges.index') }}">Colleges</a></li>
    @endcan
    @can('view', $college)
        <li class="breadcrumb-item"><a href="{{ route('colleges.show', $college) }}">{{ $college->name }}</a></li>
    @endcan

    @include('project::breadcrumb.item.current', ['title' => 'Faculty'])

@endsection

@section('reporting.title')

    {{ $college->name }}

@endsection

<?php $accordionId = 'pd-accordion' ?>
@section('reporting.accordion.card')

    @include('university::college.faculty.accordion.card.pivot')
    @include('university::college.faculty.accordion.card.faculty')

@endsection
