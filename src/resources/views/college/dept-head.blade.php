@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @can('viewAny', \Yeltrik\University\app\College::class)
                    <li class="breadcrumb-item"><a href="{{ route('colleges.index') }}">Colleges</a></li>
                @endcan
                @can('view', $college)
                    <li class="breadcrumb-item"><a href="{{ route('colleges.show', $college) }}">{{ $college->name }}</a></li>
                @endcan
                <li class="breadcrumb-item active" aria-current="page">Departments</li>
            </ol>
        </nav>

        <h1>
            {{ $college->name }}
        </h1>

        <div class="mt-4">
            @include('university::college.dept-head.table')
        </div>

    </div>
@endsection
