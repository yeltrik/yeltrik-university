@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')
    @include('university::college.breadcrumb.item.index')

@endsection

@section('reporting.title')



@endsection

<?php $accordionId = 'pd-college-accordion' ?>
@section('reporting.accordion.card')

    @include('university::college.accordion.card.pivot')
    @include('university::college.accordion.card.college')
    @include('university::college.accordion.card.summary')

@endsection
