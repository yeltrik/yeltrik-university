@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')
    @include('university::college.breadcrumb.item.show')
    @include('university::college.department.breadcrumb.item.index')

@endsection

@section('reporting.title')

    {{ $college->name }}

@endsection

<?php $accordionId = 'pd-semester-college-department-accordion' ?>
@section('reporting.accordion.card')

    @include('university::college.department.accordion.card.pivot')
    @include('university::college.department.accordion.card.department')
{{--    @include('university::college.department.accordion.card.summary')--}}

@endsection
