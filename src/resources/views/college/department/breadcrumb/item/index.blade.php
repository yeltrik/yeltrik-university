@can('viewAny', \Yeltrik\University\app\College::class)
    <li class="breadcrumb-item"><a href="{{ route('colleges.departments.index', $college) }}">Department</a></li>
@else
    <li class="breadcrumb-item active" aria-current="page">
        Department
    </li>
@endcan
