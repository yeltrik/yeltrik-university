<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center">Departments</th>
    </tr>
    </thead>
    <tbody>

    @foreach($departments as $department)
        <tr>
            <td>
                <a
                    class="btn btn-outline-light btn-block"
                    href="{{ route('departments.show', $department) }}"
                >
                    {{ $department->name }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $departments->links() }}
