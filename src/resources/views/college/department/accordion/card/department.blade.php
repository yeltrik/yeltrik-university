@extends('university::layouts.accordion.card.department', ['id' => 'pd-college-department'])

@section('accordion-card-department-body-content')

    <p>
        The following departments have faculty in {{ $college->name }}.
    </p>

    <div class="row">
        <div class="col-4 mt-4">
            <a
                class="btn btn-outline-primary btn-block disabled"
                href="#"
            >
                Unknown
            </a>
        </div>
        @foreach($departments as $department)
            <div class="col-4 mt-4">
                <a
                    class="btn btn-outline-primary btn-block"
                    href="{{ route('departments.show', [$department]) }}"
                >
                    {{ $department->name }}
                </a>
            </div>
        @endforeach
    </div>

@endsection
