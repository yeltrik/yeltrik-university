@extends('project::layouts.accordion.card.pivot', ['id' => 'college-department'])

@section('accordion-card-pivot-body-content')

    @include('project::accordion.card.pivot.tip')

    <div class="row">

        @include('project::pivot', ['text' => 'Faculty', 'route' => route('colleges.faculty.index', $college)])
        @include('project::pivot', ['text' => 'Faculty Ranks', 'route' => route('colleges.faculty-ranks.index', $college)])
        @include('project::pivot', ['text' => 'Professional Development', 'route' => route('pds.colleges.show', $college)])

    </div>

@endsection
