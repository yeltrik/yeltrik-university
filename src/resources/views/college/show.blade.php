@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')
    @include('university::college.breadcrumb.item.show')

@endsection

@section('reporting.title')

    {{ $college->name }}

@endsection

<?php $accordionId = 'pd-college-accordion' ?>
@section('reporting.accordion.card')

    @include('university::college.accordion.card.pivot-show')
    @include('university::college.accordion.card.summary-show')
{{--    @include('university::college.accordion.card.charts')--}}

@endsection
