

<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center">College</th>
        <th scope="col" class="text-center">Deans</th>
        <th scope="col" class="text-center">Departments</th>
        <th scope="col" class="text-center">Department Heads</th>
        <th scope="col" class="text-center">Faculty</th>
        <th scope="col" class="text-center">Staff</th>
        <th scope="col" class="text-center">Students</th>
    </tr>
    </thead>
    <tbody>

    @foreach($colleges as $college)
        <tr>
            <th scope="row">
                <a
                    class="btn btn-outline-light btn-block"
                    href="{{ route('colleges.show', $college) }}"
                >
                    {{ $college->name }}
                </a>
            </th>
            <td class="text-center">
                {{ $college->deans->count() }}
            </td>
            <td class="text-center">
                {{ $college->departments()->count() }}
            </td>
            <td class="text-center">
                {{ $college->departmentHeads->count() }}
            </td>
            <td class="text-center">
                {{ $college->faculty()->count() }}
            </td>
            <td class="text-center"></td>
            <td class="text-center"></td>
            {{--                    <td>--}}
            {{--                        {{ $college->professionalDevelopmentRosters()->where('attended', '=', \Yeltrik\ProfessionalDevelopment\app\ProfessionalDevelopmentRoster::ATTENDED_YES)->count()}}--}}
            {{--                    </td>--}}
        </tr>
    @endforeach
    </tbody>
</table>

{{ $colleges->links() }}
