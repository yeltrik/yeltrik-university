<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center"></th>
        <th scope="col" class="text-center">Count</th>
    </tr>
    </thead>
    <tbody>
{{--    <tr>--}}
{{--        <td class="text-center">--}}
{{--            <div class="d-flex justify-content-center">--}}
{{--                <div>Deans</div>--}}
{{--                @foreach($college->deans as $dean)--}}
{{--                    @can('view', $dean->wkuIdentity)--}}
{{--                        <div class="ml-5">--}}
{{--                            <a class="btn btn-outline-light btn-block"--}}
{{--                               href="{{ route('wku-identities.show', $dean->wkuIdentity) }}">--}}
{{--                                {{ $dean->wkuIdentity->name }}--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    @endcan--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </td>--}}
{{--        <td class="text-center">{{ $college->deans()->count() }}</td>--}}
{{--    </tr>--}}
    <tr>
        <td class="text-center">
            <a class="btn btn-outline-light btn-block"
               href="{{ route('colleges.department-head', $college) }}">
                Department Heads
            </a>
        </td>
        <td class="text-center">{{ $college->departmentHeads()->count() }}</td>
    </tr>
    <tr>
        <td>
            @can('view', $college)
                <a class="btn btn-outline-light btn-block"
                   href="{{ route('colleges.departments.index', $college) }}">
                    Departments
                </a>
            @endcan
        </td>
        <td class="text-center">{{ $college->departments()->count() }}</td>
    </tr>
    <tr>
        <td>
            @can('view', $college)
                <a class="btn btn-outline-light btn-block"
                   href="{{ route('colleges.faculty.index', $college) }}">
                    Faculty
                </a>
            @endcan
        </td>
        <td class="text-center">{{ $college->faculty()->count() }}</td>
    </tr>
    <tr>
        <td class="text-center">Staff</td>
        <td class="text-center"> ## </td>
    </tr>
    </tbody>
</table>
