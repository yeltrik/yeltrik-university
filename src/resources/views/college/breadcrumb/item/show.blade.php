@can('viewAny', \Yeltrik\University\app\College::class)
    <li class="breadcrumb-item"><a href="{{ route('colleges.index') }}">Colleges</a></li>
@endcan

@can('view', $college)
    <li class="breadcrumb-item active" aria-current="page">
        <a href="{{ route('colleges.show', $college) }}">
            {{ $college->name }}
        </a>
    </li>
@else
    <li class="breadcrumb-item active" aria-current="page">
        {{ $college->name }}
    </li>
@endcan
