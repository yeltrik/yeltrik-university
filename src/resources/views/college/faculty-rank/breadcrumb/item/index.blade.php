@can('viewAny', \Yeltrik\University\app\FacultyRank::class)
    <li class="breadcrumb-item"><a href="{{ route('faculty-ranks.index') }}">Faculty Rank</a></li>
@else
    <li class="breadcrumb-item active" aria-current="page">Faculty Rank</li>
@endcan
