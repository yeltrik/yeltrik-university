<li class="nav-item dropdown breadcrumb-item active" aria-current="page">
    <a class="" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
       aria-expanded="false">
        {{ $facultyRank->title }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        <a class="dropdown-item" href="{{ route('colleges.faculty-ranks.index', $college) }}">
            Faculty Rank
        </a>
        <div class="dropdown-divider"></div>
        @foreach($facultyRanks as $facultyRank)
            <a class="dropdown-item" href="{{ route('colleges.faculty-ranks.show', [$college, $facultyRank]) }}">
                {{ $facultyRank->title }}
            </a>
        @endforeach
    </div>
</li>
