@extends('project::layouts.reporting')

@section('reporting.breadcrumb.item')

    @include('project::breadcrumb.item.home')
    @include('university::college.breadcrumb.item.show')
    @include('university::college.faculty.breadcrumb.item.index')
    @include('university::college.faculty-rank.breadcrumb.item.index')

@endsection

@section('reporting.title')

    {{ $college->name }}

@endsection

<?php $accordionId = 'pd-college-accordion' ?>
@section('reporting.accordion.card')

    @include('university::college.faculty-rank.accordion.card.pivot')
    @include('university::college.faculty-rank.accordion.card.faculty-rank')

@endsection
