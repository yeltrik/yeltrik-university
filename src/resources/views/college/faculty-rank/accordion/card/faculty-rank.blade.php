@extends('university::layouts.accordion.card.faculty-rank', ['id' => 'college-faculty-rank'])

@section('accordion-card-faculty-rank-body-content')

    <p>
        Faculty have the following ranks for {{ $college->name }}.
    </p>

    <div class="row">
        @foreach($facultyRanks as $facultyRank)
            <div class="col-3 mt-4">
                <a
                    class="btn btn-outline-primary btn-block"
                    href="{{ route('colleges.faculty-ranks.show', [$college, $facultyRank]) }}"
                >
                    {{ $facultyRank->title }}
                </a>
            </div>
        @endforeach
    </div>

@endsection

