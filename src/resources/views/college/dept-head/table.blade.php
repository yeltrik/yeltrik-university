<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center">Department Heads</th>
        <th scope="col" class="text-center">Department</th>
    </tr>
    </thead>
    <tbody>

    @foreach($departmentHeads as $departmentHead)
        <tr>
            <td>
                <a
                    class="btn btn-outline-light btn-block"
                    href="{{ route('dept-heads.show', $departmentHead) }}"
                >
                    {{ $departmentHead->name }}
                </a>
            </td>
            <td>
                <a class="btn btn-outline-light btn-block"
                href="{{ route('departments.show', $departmentHead->department) }}">
                    {{ $departmentHead->department->name }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $departmentHeads->links() }}
