<?php $cardHeader = 'collegeSummaryHeading' ?>
<?php $cardCollapse = 'collegeSummaryCollapse' ?>
<div class="card">
    <div class="card-header" id="{{$cardHeader}}">
        <h2 class="mb-0">
            <button
                class="btn btn-link"
                type="button"
                data-toggle="collapse"
                data-target="#{{$cardCollapse}}"
                aria-expanded="true"
                aria-controls="{{$cardCollapse}}"
            >
                Charts
            </button>
        </h2>
    </div>

    <div
        id="{{$cardCollapse}}"
        class="collapse show"
        aria-labelledby="{{$cardHeader}}"
        data-parent="#{{$accordionId}}"
    >
        <div class="card-body">

{{--            <div class="row">--}}
{{--                <div class="col-12">--}}
{{--                    @include('university::college.summary.chart-js.department-attendance')--}}
{{--                </div>--}}
{{--            </div>--}}

        </div>
    </div>
</div>
