@extends('project::layouts.accordion.card.summary', ['id' => 'college-show'])

@section('accordion-card-summary-body-content')

    @include('university::college.table.summary-show')

@endsection
