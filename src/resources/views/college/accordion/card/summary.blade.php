@extends('project::layouts.accordion.card.summary', ['id' => 'college'])

@section('accordion-card-summary-body-content')

    @include('university::college.table.summary')

@endsection
