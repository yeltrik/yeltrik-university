@extends('project::layouts.accordion.card.pivot', ['id' => 'college'])

@section('accordion-card-pivot-body-content')

    @include('project::accordion.card.pivot.tip')

    <div class="row">

        @include('project::pivot', ['text' => 'Professional Development', 'route' => route('pds.index')])

    </div>

@endsection
