@extends('university::layouts.accordion.card.college', ['id' => 'college'])

@section('accordion-card-college-body-content')

    <p>
        The following colleges have reportable PD participation.
    </p>
    <div class="row">
        @foreach($colleges as $college)
            <div class="col-4 mt-4">
                <a
                    class="btn btn-outline-primary btn-block"
                    href="{{ route('colleges.show', [$college]) }}"
                >
                    {{ $college->name }}
                </a>
            </div>
        @endforeach
    </div>

@endsection
