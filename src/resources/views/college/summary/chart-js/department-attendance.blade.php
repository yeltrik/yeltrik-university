<?php $chartId = "demographicDepartmentChart" ?>
<?php $datasetLabel = "Attendance by Department" ?>
<?php $data = $departmentAttendanceData; ?>
<?php $colors = []; ?>
@include('project::chartjs.horizontal-bar.simple')
