@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @can('viewAny', Yeltrik\University\app\DepartmentHead::class)
                    <li class="breadcrumb-item">
                        <a href="{{ route('dept-heads.index') }}">
                            Department Heads
                        </a>
                    </li>
                @endcan
                <li class="breadcrumb-item active" aria-current="page">{{ $departmentHead->title }}</li>
            </ol>
        </nav>

        @if( $departmentHead->department->college instanceof \Yeltrik\University\app\College )
            <h5>

                @can('view', $departmentHead->department->college)
                    <a
                        class="text-dark"
                        href="{{ route('colleges.show', $departmentHead->department->college) }}"
                    >
                        {{ $departmentHead->department->college->name }}
                    </a>
                @else
                    {{ $departmentHead->department->college->name }}
                @endcan
            </h5>
        @endif

        <h3>
            <a
                class="text-dark"
                href="{{ route('departments.show', $departmentHead->department) }}"
            >
                {{ $departmentHead->department->name }}
            </a>
        </h3>

        <div class="d-flex justify-content-between">
            <div>
                <h1>
                    @can('view', $departmentHead)
                        @include('university::link.dept-head', ['departmentHead' => $departmentHead])
                    @endcan
                    {{ $departmentHead->title }}
                </h1>
            </div>

            <div class="d-flex float-right">
                @include('university::dept-head.icons')
            </div>
        </div>

        {{--@include('pd.roster.table.session')--}}

    </div>
@endsection
