@if($departmentHead->wkuIdentity()->exists())

    @if($departmentHead->wkuIdentity->isAsana())
        @can('view-asana')
            <div class="ml-2">
                @include('project::link.asana', ['wkuIdentity' => $departmentHead->wkuIdentity])
            </div>
        @endcan
    @endif

    @if($departmentHead->wkuIdentity->isDean())
        @can('view', $departmentHead->wkuIdentity->dean)
            <div class="ml-2">
                @include('university::link.dean', ['dean' => $departmentHead->wkuIdentity->dean])
            </div>
        @endcan
    @endif

    @if($departmentHead->wkuIdentity->isCitlStaff())
        @can('view', $departmentHead->wkuIdentity->citlStaff)
            <div class="ml-2">
                @include('universityDepartment::link.citl-staff', ['citlStaff' => $departmentHead->wkuIdentity->citlStaff])
            </div>
        @endcan
    @endif

    @if($departmentHead->wkuIdentity->isFaculty())
        <div class="ml-2">
            @can('view', $departmentHead->wkuIdentity->faculty)
                @include('university::link.faculty', ['faculty' => $departmentHead->wkuIdentity->faculty])
            @endcan
        </div>
    @endif

    @if($departmentHead->wkuIdentity->user()->exists())
        @can('view', $departmentHead->wkuIdentity->user)
            <div class="ml-2">
                @include('project::link.user', ['user' => $departmentHead->wkuIdentity->user])
            </div>
        @endcan
    @endif

    @can('view', $departmentHead->wkuIdentity)
        <div class="ml-2">
            @include('university::link.wku-identity', ['wkuIdentity' => $departmentHead->wkuIdentity])
        </div>
    @endcan

@endif
