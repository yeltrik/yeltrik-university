<div class="list-group">
    <button type="button" class="list-group-item list-group-item-action active">
        Department Heads
    </button>
    @foreach($departmentHeads as $departmentHead)
        <a
            class="list-group-item list-group-item-action"
            href="{{ route('dept-heads.show', $departmentHead) }}"
        >
            {{ $departmentHead->title }}
        </a>
    @endforeach
</div>
