@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Department Head</li>
            </ol>
        </nav>

        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col" class="text-center">College</th>
                <th scope="col" class="text-center">Department</th>
                <th scope="col" class="text-center">Department Head</th>
            </tr>
            </thead>
            <tbody>

            @foreach($departmentHeads as $departmentHead)
                <tr>
                    <td>
                        @if( $departmentHead->department->college instanceof \Yeltrik\University\app\College )
                        <a
                            class="btn btn-outline-light btn-block"
                            href="{{ route('colleges.show', $departmentHead->department->college) }}"
                        >
                            {{ $departmentHead->department->college->name }}
                        </a>
                        @endif
                    </td>
                    <td>
                        <a
                            class="btn btn-outline-light btn-block"
                            href="{{ route('departments.show', $departmentHead->department) }}"
                        >
                            {{ $departmentHead->department->name }}
                        </a>
                    </td>
                    <th scope="row">
                        <a
                            class="btn btn-outline-light btn-block"
                            href="{{ route('dept-heads.show', $departmentHead) }}"
                        >
                            {{ $departmentHead->title }}
                        </a>
                    </th>
                </tr>
            @endforeach

            </tbody>
        </table>

        {{ $departmentHeads->links() }}

    </div>
@endsection
