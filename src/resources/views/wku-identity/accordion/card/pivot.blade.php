<?php $cardHeader = 'wku-identity-pivot-heading' ?>
<?php $cardCollapse = 'wku-identity-pivot-collapse' ?>
<div class="card">
    <div class="card-header" id="{{$cardHeader}}">
        <h2 class="mb-0">
            <button
                class="btn btn-link"
                type="button"
                data-toggle="collapse"
                data-target="#{{$cardCollapse}}"
                aria-expanded="true"
                aria-controls="{{$cardCollapse}}"
            >
                Pivot Data
            </button>
        </h2>
    </div>

    <div
        id="{{$cardCollapse}}"
        class="collapse show"
        aria-labelledby="{{$cardHeader}}"
        data-parent="#{{$accordionId}}"
    >
        <div class="card-body">
            <div class="list-group mb-3">

                <div class="row">

                    @if($hasPdRoster)
                        <div class="col-4">
                            <a href="{{ route('pds.wku-identities.rosters.index', $wkuIdentity) }}"
                               class="btn btn-outline-primary btn-block">
                                @include('project::icon.pivot-data')
                                Professional Development Attendance
                            </a>
                        </div>
                    @endif

                </div>
            </div>
        </div>

    </div>
</div>
