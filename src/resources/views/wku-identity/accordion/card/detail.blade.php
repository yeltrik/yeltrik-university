<?php $cardHeader = 'wku-identity-details-heading' ?>
<?php $cardCollapse = 'wku-identity-details-collapse' ?>
<div class="card">
    <div class="card-header" id="{{$cardHeader}}">
        <h2 class="mb-0">
            <button
                class="btn btn-link"
                type="button"
                data-toggle="collapse"
                data-target="#{{$cardCollapse}}"
                aria-expanded="true"
                aria-controls="{{$cardCollapse}}"
            >
                Details
            </button>
        </h2>
    </div>

    <div
        id="{{$cardCollapse}}"
        class="collapse show"
        aria-labelledby="{{$cardHeader}}"
        data-parent="#{{$accordionId}}"
    >
        <div class="card-body">
            <div class="list-group mb-3">

                @include('university::wku-identity.index-detail')

            </div>
        </div>

    </div>
</div>
