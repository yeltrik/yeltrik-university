<h2>

    <div class="row">
        <div class="col-4 text-right">
            Name:
        </div>
        <div class="col-6">
            {{ $wkuIdentity->name }}
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-4 text-right">
            Email:
        </div>
        <div class="col-6">
            {{ $wkuIdentity->email }}
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-4 text-right">
            WKU NetId:
        </div>
        <div class="col-6">
            {{ $wkuIdentity->netid }}
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-4 text-right">
            WKU ID:
        </div>
        <div class="col-6">
            {{ $wkuIdentity->wkuid }}
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-4 text-right">
            College:
        </div>
        <div class="col-6">
            @if($wkuIdentity->college()->exists())
                @can('view', $wkuIdentity->college)
                    <a
                        class="text-dark"
                        href="{{ route('colleges.show', $wkuIdentity->college) }}"
                    >
                        {{ $wkuIdentity->college->name }}
                    </a>
                @else
                    {{ $wkuIdentity->college->name }}
                @endcan
            @endif
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-4 text-right">
            Department:
        </div>
        <div class="col-6">
            @if($wkuIdentity->department()->exists())
                @can('view', $wkuIdentity->department)
                    <a
                        class="text-dark"
                        href="{{ route('departments.show', $wkuIdentity->department) }}"
                    >
                        {{ $wkuIdentity->department->name }}
                    </a>
                @else
                    {{ $wkuIdentity->department->name }}
                @endcan
            @endif
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-4 text-right">
            Asana:
        </div>
        <div class="col-6">
            <a class="btn btn-outline-info" href="{{ $wkuIdentity->asanaHref() }}" target="_blank">
                @include('project::icon.asana')
            </a>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-4 text-right">
            User:
        </div>
        <div class="col-6">
            @can('view', $wkuIdentity->user)
                @include('project::link.user', ['user' => $wkuIdentity->user])
            @endcan
        </div>
    </div>

</h2>
