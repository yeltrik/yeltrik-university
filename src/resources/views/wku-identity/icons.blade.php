@if($wkuIdentity->isAsana())
    @can('view-asana')
        <div class="ml-2">
            @include('project::link.asana')
        </div>
    @endcan
@endif

@if($wkuIdentity->isDean())
    @can('view', $wkuIdentity->dean)
        <div class="ml-2">
            @include('university::link.dean', ['dean' => $wkuIdentity->dean])
        </div>
    @endcan
@endif

@if($wkuIdentity->isDepartmentHead())
    @can('view', $wkuIdentity->departmentHead)
        <div class="ml-2">
            @include('university::link.dept-head', ['departmentHead' => $wkuIdentity->departmentHead])
        </div>
    @endcan
@endif

@if($wkuIdentity->isCitlStaff())
    <div class="ml-2">
        @can('view', $wkuIdentity->citlStaff)
            @include('universityDepartment::link.citl-staff', ['citlStaff' => $wkuIdentity->citlStaff])
        @endcan
    </div>
@endif

@if($wkuIdentity->isFaculty())
    <div class="ml-2">
        @can('view', $wkuIdentity->faculty)
            @include('university::link.faculty', ['faculty' => $wkuIdentity->faculty])
        @endcan
    </div>
@endif

@if($wkuIdentity->user()->exists())
    @can('view', $wkuIdentity->user)
        <div class="ml-2">
            @include('project::link.user', ['user' => $wkuIdentity->user])
        </div>
    @endcan
@endif
