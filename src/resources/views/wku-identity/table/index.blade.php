<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Email</th>
        <th scope="col" class="text-center">NetID</th>
        <th scope="col" class="text-center">WKUID</th>
        <th scope="col" class="text-center">Profiles</th>
    </tr>
    </thead>
    <tbody>

    @foreach($wkuIdentities as $wkuIdentity)
        <tr>
            <td>
                <a
                    class="btn btn-outline-light btn-block"
                    href="{{ route('wku-identities.show', $wkuIdentity) }}">
                    {{ $wkuIdentity->name }}
                </a>
            </td>
            <td class="text-center">{{ $wkuIdentity->email }}</td>
            <td class="text-center">{{ $wkuIdentity->netid }}</td>
            <td class="text-center">{{ $wkuIdentity->wkuid }}</td>
            <th scope="row">
                <div class="d-flex justify-content-center">
                    <div>
                        <a
                            class="btn btn-outline-info"
                            href="{{ route('wku-identities.show', $wkuIdentity) }}"
                        >
                            @include('university::icon.wku-identity')
                        </a>
                    </div>
                    @include('university::wku-identity.icons')
                </div>
            </th>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $wkuIdentities->links() }}
