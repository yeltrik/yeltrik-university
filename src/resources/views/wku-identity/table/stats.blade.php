<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th scope="col" class="text-center"></th>
        <th scope="col" class="text-center">Count</th>
    </tr>
    </thead>
    <tbody>

        <tr>
            <th class="text-center">
                With Email
            </th>
            <td class="text-center">
                {{ $countWithEmail }}
            </td>
        </tr>

        <tr>
            <th class="text-center">
                With NetId
            </th>
            <td class="text-center">
                {{ $countWithNetId }}
            </td>
        </tr>

        <tr>
            <th class="text-center">
                With WKU ID
            </th>
            <td class="text-center">
                {{ $countWithWkuId }}
            </td>
        </tr>

        <tr>
            <th class="text-center">
                With User Profile
            </th>
            <td class="text-center">
                {{ $countWithUserId }}
            </td>
        </tr>

        <tr>
            <th class="text-center">
                With Asana
            </th>
            <td class="text-center">
                {{ $countWithAsana }}
            </td>
        </tr>

        <tr>
            <th class="text-center">
                With College
            </th>
            <td class="text-center">
                {{ $countWithCollege }}
            </td>
        </tr>

        <tr>
            <th class="text-center">
                With Department
            </th>
            <td class="text-center">
                {{ $countWithDepartment }}
            </td>
        </tr>

    </tbody>
</table>
