@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">WKU Identities</li>
            </ol>
        </nav>

        @include('university::wku-identity.table.stats')

        @include('university::wku-identity.table.index')

    </div>
@endsection
