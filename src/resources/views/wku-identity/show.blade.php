@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @can('viewAny', \Yeltrik\University\app\WkuIdentity::class)
                    <li class="breadcrumb-item">
                        <a href="{{ route('wku-identities.index') }}">
                            WKU Identities
                        </a>
                    </li>
                @endcan
                <li class="breadcrumb-item active" aria-current="page">{{ $wkuIdentity->name }}</li>
            </ol>
        </nav>

        <div class="d-flex justify-content-between">
            <div>
                <h1>
                    @can('view', $wkuIdentity)
                        @include('university::link.wku-identity')
                    @endcan

                    {{ $wkuIdentity->name }}
                </h1>
            </div>

            <div class="d-flex float-right">
                @include('university::wku-identity.icons')
            </div>
        </div>

        <?php $accordionId = 'wku-identity-Accordion' ?>
        <div class="accordion" id="{{$accordionId}}">

            @include('university::wku-identity.accordion.card.pivot')
            @include('university::wku-identity.accordion.card.detail')

        </div>

    </div>
@endsection
