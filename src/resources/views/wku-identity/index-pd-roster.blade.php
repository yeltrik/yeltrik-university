<nav>
    <div
        class="nav nav-tabs" role="tablist"
        id="nav-roster-tab"
    >
        <a
            class="nav-item nav-link active text-center col-6" data-toggle="tab" role="tab"
            id="nav-attended-tab" href="#nav-attended" aria-controls="nav-attended"
            aria-selected="true"
        >
            @include('professionalDevelopment::roster.attended.icon-yes')
            Attended
        </a>
        <a class="nav-item nav-link text-center col-6" data-toggle="tab" role="tab"
           id="nav-not-attended-tab" href="#nav-not-attended" aria-controls="nav-not-attended"
           aria-selected="false"
        >
            @include('professionalDevelopment::roster.attended.icon-no')
            Registered / Not Attended
        </a>
    </div>
</nav>

<div
    class="tab-content"
    id="nav-tabContent"
>
    <div
        class="tab-pane fade show active" role="tabpanel"
        id="nav-attended" aria-labelledby="nav-attended-tab"
    >
        @include('professionalDevelopment::roster.table.session', [ 'professionalDevelopmentRosters' => $professionalDevelopmentRostersAttended ])
    </div>
    <div
        class="tab-pane fade" role="tabpanel"
        id="nav-not-attended" aria-labelledby="nav-not-attended-tab"
    >
        @include('professionalDevelopment::roster.table.session', [ 'professionalDevelopmentRosters' => $professionalDevelopmentRostersNotAttended ])
    </div>
</div>
