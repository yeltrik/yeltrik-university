@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @can('viewAny', \Yeltrik\University\app\WkuIdentity::class)
                    <li class="breadcrumb-item">
                        <a href="{{ route('wku-identities.index') }}">
                            WKU Identities
                        </a>
                    </li>
                @endcan
                <li class="breadcrumb-item"><a href="{{ route('wku-identities.show', $wkuIdentity) }}">{{ $wkuIdentity->name }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">Roster</li>
            </ol>
        </nav>

        <p>
            Below are the CITL Professional Development Roster records for {{ $wkuIdentity->name }}.
        </p>

        @include('wku-identity.index-pd-roster')

    </div>
@endsection
