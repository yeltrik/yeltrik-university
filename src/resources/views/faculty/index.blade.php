@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Faculty</li>
            </ol>
        </nav>


        <?php $accordionId = 'facultyAccordion' ?>
        <div class="accordion" id="{{$accordionId}}">

            @include('university::faculty.accordion.card.pivot')
            @include('university::faculty.accordion.card.raw')
            {{--            @include('university::faculty.accordion.card.summary')--}}

        </div>

    </div>
@endsection
