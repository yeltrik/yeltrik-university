@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @can('viewAny', \Yeltrik\University\app\Faculty::class)
                    <li class="breadcrumb-item">
                        <a href="{{ route('faculty.index') }}">
                            Faculty
                        </a>
                    </li>
                @endcan
                <li class="breadcrumb-item active" aria-current="page">{{ $faculty->title }}</li>
            </ol>
        </nav>

        <div class="d-flex justify-content-between">
            <div>
                <h1>
                    @can('view', $faculty->wkuIdentity->faculty)
                        @include('university::link.faculty', ['faculty' => $faculty->wkuIdentity->faculty])
                    @endcan
                    {{ $faculty->title }}
                </h1>
            </div>

            <div class="d-flex float-right">
                @include('university::faculty.icons')
            </div>
        </div>

        {{--@include('pd.roster.table.session')--}}

    </div>
@endsection
