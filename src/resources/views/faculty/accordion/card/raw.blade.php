<?php $cardHeader = 'professionalDevelopmentRawHeading' ?>
<?php $cardCollapse = 'professionalDevelopmentRawCollapse' ?>
<div class="card">
    <div class="card-header" id="{{$cardHeader}}">
        <h2 class="mb-0">
            <button
                class="btn btn-link"
                type="button"
                data-toggle="collapse"
                data-target="#{{$cardCollapse}}"
                aria-expanded="true"
                aria-controls="{{$cardCollapse}}"
            >
                List
            </button>
        </h2>
    </div>

    <div
        id="{{$cardCollapse}}"
        class="collapse show"
        aria-labelledby="{{$cardHeader}}"
        data-parent="#{{$accordionId}}"
    >
        <div class="card-body">

            @if($faculties->count() > 0)
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Rank</th>
                        <th scope="col">Profiles</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($faculties as $faculty)
                        <tr>
                            <th scope="row">
                                <a
                                    class="btn btn-outline-light btn-block"
                                    href="{{ route('faculty.show', $faculty) }}"
                                >
                                    {{ $faculty->title }}
                                </a>
                            </th>
                            <td>
                                @if($faculty->facultyRank()->exists())
                                    <a
                                        class="btn btn-light btn-block"
                                        href="{{ route('faculty-ranks.show', $faculty->facultyRank->id) }}"
                                    >
                                        {{ $faculty->faculty_rank_title }}
                                    </a>
                                @endif
                            </td>
                            <td>
                                <div class="d-flex">
                                    @include('university::link.faculty')
                                    @include('university::faculty.icons')
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $faculties->links() }}
            @else
                <h2>
                    There are no Faculty in the System.
                </h2>
            @endif


        </div>

    </div>
</div>
