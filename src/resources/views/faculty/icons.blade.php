@if($faculty->wkuIdentity()->exists())

    @if($faculty->wkuIdentity->isAsana())
        @can('view-asana')
            <div class="ml-2">
                @include('project::link.asana', ['wkuIdentity' => $faculty->wkuIdentity])
            </div>
        @endcan
    @endif

    @if($faculty->wkuIdentity->isDean())
        @can('view', $faculty->wkuIdentity->dean)
            <div class="ml-2">
                @include('university::link.dean', ['dean' => $faculty->wkuIdentity->dean])
            </div>
        @endcan
    @endif

    @if($faculty->wkuIdentity->isDepartmentHead())
        @can('view', $faculty->wkuIdentity->departmentHead)
            <div class="ml-2">
                @include('university::link.dept-head', ['departmentHead' => $faculty->wkuIdentity->departmentHead])
            </div>
        @endcan
    @endif

    @if($faculty->wkuIdentity->isCitlStaff())
        <div class="ml-2">
            @can('view', $faculty->wkuIdentity->citlStaff)
                @include('universityDepartment::link.citl-staff', ['citlStaff' => $faculty->wkuIdentity->citlStaff])
            @endcan
        </div>
    @endif

    @if($faculty->wkuIdentity->user()->exists())
        @can('view', $faculty->wkuIdentity->user)
            <div class="ml-2">
                @include('project::link.user', ['user' => $faculty->wkuIdentity->user])
            </div>
        @endcan
    @endif

    @can('view', $faculty->wkuIdentity)
        <div class="ml-2">
            @include('university::link.wku-identity', ['wkuIdentity' => $faculty->wkuIdentity])
        </div>
    @endcan

@endif
