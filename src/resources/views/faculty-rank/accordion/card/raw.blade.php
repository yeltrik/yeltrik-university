<?php $cardHeader = 'professionalDevelopmentFacultyRankRawHeading' ?>
<?php $cardCollapse = 'professionalDevelopmentFacultyRankRawCollapse' ?>
<div class="card">
    <div class="card-header" id="{{$cardHeader}}">
        <h2 class="mb-0">
            <button
                class="btn btn-link"
                type="button"
                data-toggle="collapse"
                data-target="#{{$cardCollapse}}"
                aria-expanded="true"
                aria-controls="{{$cardCollapse}}"
            >
                Summary
            </button>
        </h2>
    </div>

    <div
        id="{{$cardCollapse}}"
        class="collapse show"
        aria-labelledby="{{$cardHeader}}"
        data-parent="#{{$accordionId}}"
    >
        <div class="card-body">

            @if($facultyRanks->count() > 0)
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col" class="text-center">Rank</th>
                        <th scope="col" class="text-center">Count</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($facultyRanks as $facultyRank)
                        <tr>
                            <th scope="row">
                                <a
                                    class="btn btn-outline-light btn-block"
                                    href="{{ route('faculty-ranks.show', $facultyRank) }}"
                                >
                                    {{ $facultyRank->title }}
                                </a>
                            </th>
                            <td class="text-center">
                                {{ $facultyRank->faculties->count() }}
{{--                                <a--}}
{{--                                    class="btn btn-light btn-block"--}}
{{--                                    href="{{ route('faculty-ranks.show', $facultyRank) }}"--}}
{{--                                >--}}
{{--                                    {{ $facultyRank->faculty_rank_title }}--}}
{{--                                </a>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $facultyRanks->links() }}
            @else
                <h2>
                    There are no Faculty Ranks in the System.
                </h2>
            @endif

        </div>

    </div>
</div>
