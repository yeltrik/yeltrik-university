<?php $cardHeader = 'professionalDevelopmentFacultyRankPivotHeading' ?>
<?php $cardCollapse = 'professionalDevelopmentFacultyRankPivotCollapse' ?>
<div class="card">
    <div class="card-header" id="{{$cardHeader}}">
        <h2 class="mb-0">
            <button
                class="btn btn-link"
                type="button"
                data-toggle="collapse"
                data-target="#{{$cardCollapse}}"
                aria-expanded="true"
                aria-controls="{{$cardCollapse}}"
            >
                Pivot Data
            </button>
        </h2>
    </div>

    <div
        id="{{$cardCollapse}}"
        class="collapse show"
        aria-labelledby="{{$cardHeader}}"
        data-parent="#{{$accordionId}}"
    >
        <div class="card-body">
            <div class="list-group mb-3">
                <div class="row">
                    <div class="col-4">
                        <a href="{{ route('colleges.index') }}" class="btn btn-outline-primary btn-block">
                            @include('project::icon.pivot-data')
                            College
                        </a>
                    </div>

                    <div class="col-4">
                        <a href="{{ route('faculty.index') }}" class="btn btn-outline-primary btn-block">
                            @include('project::icon.pivot-data')
                            Faculty
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
