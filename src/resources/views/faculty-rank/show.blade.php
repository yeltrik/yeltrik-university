@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                @can('viewAny', \Yeltrik\University\app\Faculty::class)
                    <li class="breadcrumb-item">
                        <a href="{{ route('faculty-ranks.index') }}">
                            Faculty Rank
                        </a>
                    </li>
                @endcan
                <li class="breadcrumb-item active" aria-current="page">{{ $facultyRank->title }}</li>
            </ol>
        </nav>

        @foreach($faculties as $faculty)
            <div>
                <a
                    class=""
                    href="{{ route('faculty.show', $faculty) }}"
                >
                    {{ $faculty->title }}
                </a>
            </div>
        @endforeach
        {{ $faculties }}

    </div>
@endsection
