@extends('project::layouts.app')

@section('content')
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Faculty Rank</li>
            </ol>
        </nav>

        <?php $accordionId = 'facultyRankAccordion' ?>
        <div class="accordion" id="{{$accordionId}}">

            @include('university::faculty-rank.accordion.card.pivot')
            @include('university::faculty-rank.accordion.card.raw')
            @include('university::faculty-rank.accordion.card.summary')

        </div>

    </div>
@endsection
