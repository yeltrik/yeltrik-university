<a
    class="btn btn-outline-info"
    href="{{ route('dept-heads.show', $departmentHead) }}"
>
    @include('university::icon.dept-head')
</a>
