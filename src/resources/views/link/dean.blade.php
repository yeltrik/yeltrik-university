<a
    class="btn btn-outline-info"
    href="{{ route('deans.show', $dean) }}"
>
    @include('university::icon.dean')
</a>
