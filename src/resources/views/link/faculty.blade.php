<a
    class="btn btn-outline-info"
    href="{{ route('faculty.show', $faculty) }}"
>
    @include('university::icon.faculty')
</a>
