<a
    class="btn btn-outline-info"
    href="{{ route('wku-identities.show', $wkuIdentity) }}"
>
    @include('university::icon.wku-identity')
</a>
