<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemestersTable extends Migration
{

    public static $terms = [
        'Fall',
        'Winter',
        'Summer',
        'Spring',
        'On Demand',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('university')->create('semesters', function (Blueprint $table) {
            $table->id();
            $table->integer('year');
            $table->enum('term', static::$terms);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('university')->dropIfExists('semesters');
    }
}
