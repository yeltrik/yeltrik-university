<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Yeltrik\University\app\models\DepartmentHead;

class DepartmentHeadSeeder extends Seeder
{

    use \App\AsanaExportSeeding_Trait;
    use \App\AsanaTaskCustomField_Trait;

    const ASANA_JSON_EXPORT_PATH = 'dept_heads.json';

    const USER_PASSWORD = 'WkuCitl20!';

    // TASK KEYS
    //array (
    //    0 => 'gid',
    //    1 => 'assignee',
    //    2 => 'assignee_status',
    //    3 => 'completed',
    //    4 => 'completed_at',
    //    5 => 'created_at',
    //    6 => 'custom_fields',
    //    7 => 'due_at',
    //    8 => 'due_on',
    //    9 => 'followers',
    //    10 => 'hearted',
    //    11 => 'hearts',
    //    12 => 'liked',
    //    13 => 'likes',
    //    14 => 'memberships',
    //    15 => 'modified_at',
    //    16 => 'name',
    //    17 => 'notes',
    //    18 => 'num_hearts',
    //    19 => 'num_likes',
    //    20 => 'parent',
    //    21 => 'projects',
    //    22 => 'resource_type',
    //    23 => 'start_on',
    //    24 => 'subtasks',
    //    25 => 'tags',
    //    26 => 'resource_subtype',
    //    27 => 'workspace',
    //)

    // CUSTOM FIELD KEYS
    //First Name
    //Last Name
    //Email
    //WKU ID
    //NetID
    //Teaching Status
    //College
    //Department
    //Staff Department(s)
    //Faculty Rank
    //Highest Degree

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(Storage::disk('public')->get(static::ASANA_JSON_EXPORT_PATH), true);

        foreach ($json as $data) {
            foreach ($data as $key => $task) {
                $name = $task['name'];

                $customFields = $task['custom_fields'];

                $asanaGid = $task['gid'];
                $email = strtolower(static::getCustomFieldTextValue($customFields, 'Email'));
                $wkuId = static::getCustomFieldNumberValue($customFields, 'WKU ID');
                $netId = static::getCustomFieldTextValue($customFields, 'NetID');
                $teachingStatus = static::getCustomFieldEnumValue($customFields, 'Teaching Status');
                $facultyRank = static::getCustomFieldEnumValue($customFields, 'Faculty Rank');
                $highestDegree = static::getCustomFieldEnumValue($customFields, 'Highest Degree');
                $collegeName = static::getCustomFieldEnumValue($customFields, 'College');
                $departmentName = static::getCustomFieldEnumValue($customFields, 'Department');

                $college = $this->getCollege($collegeName);
                $department = $this->getDepartment($departmentName, $college);

                if ($email != NULL) {
                    $user = $this->getUser($name, $email, static::USER_PASSWORD);
                    $wkuIdentity = $this->getWkuIdentity($user, $name, $email, $netId, $wkuId, $asanaGid, $college, $department);

                    // Create Department Head
                    $departmentHead = new DepartmentHead();
                    $departmentHead->department()->associate($department);
                    $departmentHead->wkuIdentity()->associate($wkuIdentity);
                    $departmentHead->save();

                    //$this->command->info("Department Head: " . $name);
                } else {
                    $this->command->warn('Skipping Department Head: ' . $name);
                }
            }
        }
    }

}
