<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Yeltrik\University\app\models\Department;

class FacultySeeder extends Seeder
{

    const ASANA_CSV_EXPORT_PATH = 'faculty.csv';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('public')->get(static::ASANA_CSV_EXPORT_PATH);
        $bom = pack('CCC', 0xEF, 0xBB, 0xBF);
        if (strncmp($data, $bom, 3) === 0) {
            $data = substr($data, 3);
        }
        $rows = explode("\n", $data);
        $array = $this->csv($rows);

        foreach ($array as $row) {
            if (array_key_exists('Projects', $row)) {
                $projects = $row['Projects'];
                if (stristr($projects, 'Faculty')) {
                    $gidKey = 'Task ID';
                    $asanaGid = $row[$gidKey];

                    $name = $row['Name'];
                    $email = strtolower($row['Email']);
                    $netId = strtolower($row['NetID']);
                    $wkuId = $row['WKU ID'];
                    $collegeName = $row['College'];
                    $departmentName = $row['Department'];
                    $facultyRankTitle = $row['Faculty Rank'];

                    $query = \Yeltrik\University\app\WkuIdentity::query();
                    if ($asanaGid != NULL) {
                        $query->orWhere('asana_gid', '=', $asanaGid);
                    }
                    if ($name != NULL) {
                        $query->orWhere('name', '=', $name);
                    }
                    if ($email != NULL) {
                        $query->orWhere('email', '=', $email);
                    }
                    if ($netId != NULL) {
                        $query->orWhere('netid', '=', $netId);
                    }
                    if ($wkuId != NULL) {
                        $query->orWhere('wkuid', '=', $wkuId);
                    }

                    $wkuIdentity = $query->first();

                    if ($wkuIdentity instanceof \Yeltrik\University\app\WkuIdentity) {
                        //dd($wkuIdentity);
                    } else {
                        $wkuIdentity = new \Yeltrik\University\app\WkuIdentity();
                        if ($asanaGid != NULL) {
                            $wkuIdentity->asana_gid = $asanaGid;
                        }
                        if ($name != NULL) {
                            $wkuIdentity->name = $name;
                        }
                        if ($email != NULL) {
                            $wkuIdentity->email = $email;
                        }
                        if ($netId != NULL) {
                            $wkuIdentity->netid = $netId;
                        }
                        if ($wkuId != NULL) {
                            $wkuIdentity->wkuid = $wkuId;
                        }

                        $wkuIdentity->save();

                        if ($collegeName != NULL) {
                            $college = \Yeltrik\University\app\College::query()->where('name', '=', $collegeName)->first();
                            if ($college === NULL) {
                                $college = new \Yeltrik\University\app\College();
                                $college->name = $collegeName;
                                $college->save();
                            }
                            $wkuIdentity->college()->associate($college);
                            $wkuIdentity->save();
                        }
                        if ($departmentName != NULL) {
                            // Why are we never getting into here?
                            // THERE are multiple Department Fields in the CSV
                            //dd($departmentName);
                            $department = Department::query()->where('name', '=', $departmentName)->first();
                            if ($department === NULL) {
                                $department = new Department();
                                $department->name = $departmentName;
                                $department->save();
                            }

                            if ( $department->college()->exists() === FALSE && $college instanceof \Yeltrik\University\app\College ) {
                                $department->college()->associate($college);
                                $department->save();
                            }

                            $wkuIdentity->department()->associate($department);
                            $wkuIdentity->save();
                        }
                    }

                    unset($user);
                    if ($email != NULL) {
                        $query = \App\User::query();
                        if ($name != NULL) {
                            $query->orWhere('name', '=', $name);
                        }
                        if ($email != NULL) {
                            $query->orWhere('email', '=', $email);
                        }
                        $user = $query->first();
                        if ($user instanceof \App\User) {

                        } else {
                            $user = new \App\User();
                            if ($name != NULL) {
                                $user->name = $name;
                            }
                            if ($email != NULL) {
                                $user->email = $email;
                            }
                            $user->password = Hash::make(UserSeeder::USER_PASSWORD);
                            $user->save();
                        }
                    }

                    $faculty = $wkuIdentity->faculty;
                    if ($faculty instanceof \Yeltrik\University\app\Faculty) {

                    } else {
                        $faculty = new \Yeltrik\University\app\Faculty();
                        $faculty->wkuIdentity()->associate($wkuIdentity);
                        if (isset($user) && $user instanceof \App\User) {
                            $faculty->user()->associate($user);
                        }
                        $faculty->save();
                    }

                    if  ($facultyRankTitle != "") {
                        $facultyRank = \Yeltrik\University\app\FacultyRank::query()->where('title', '=', $facultyRankTitle)->first();
                        if ( $facultyRank instanceof \Yeltrik\University\app\FacultyRank === FALSE ) {
                            $facultyRank = new \Yeltrik\University\app\FacultyRank();
                            $facultyRank->title = $facultyRankTitle;
                            $facultyRank->save();
                        }
                        $faculty->facultyRank()->associate($facultyRank);
                        $faculty->save();
                    }

                    //$this->command->info('Name: ' . $name);
                } else {
                    $this->command->warn('Skip: ' . $projects);
                }
            } else {
                $this->command->alert('Corruption?: ');
            }
        }
    }

    private function csv($rows)
    {
        $array = array_map('str_getcsv', $rows);
        array_walk($array, function (&$a) use ($array) {
            if (sizeof($array[0]) === sizeof($a)) {
                $a = array_combine($array[0], $a);
            }
        });
        array_shift($array); # remove column header
        return $array;
    }

}
