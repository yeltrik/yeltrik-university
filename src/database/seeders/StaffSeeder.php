<?php

use Illuminate\Database\Seeder;
use Yeltrik\University\app\models\Staff;

class StaffSeeder extends Seeder
{

    use \App\AsanaExportSeeding_Trait;
    use \App\AsanaTaskCustomField_Trait;

    CONST ASANA_JSON_EXPORT_PATH = 'wku_staff.json';

    CONST USER_PASSWORD = 'WkuCitl20!';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(Storage::disk('public')->get(static::ASANA_JSON_EXPORT_PATH), true);

        foreach($json as $data) {
            foreach ($data as $key => $task) {
                $name = $task['name'];

                $customFields = $task['custom_fields'];

                $asanaGid = $task['gid'];
                $email = strtolower(static::getCustomFieldTextValue($customFields, 'Email'));
                $netId = static::getCustomFieldTextValue($customFields, 'NetID');
                $wkuId = static::getCustomFieldNumberValue($customFields, 'WKU ID');

                if ($email != NULL) {
                    $user = $this->getUser($name, $email, static::USER_PASSWORD);
                } else {
                    $user = NULL;
                }

                $wkuIdentity = $this->getWkuIdentity($user, $name, $email, $netId, $wkuId, $asanaGid);

                $staff = new Staff();
                $staff->wkuIdentity()->associate($wkuIdentity);
                $staff->save();

                //$this->command->info("WKU Staff: " . $name);
            }
        }

    }

}
