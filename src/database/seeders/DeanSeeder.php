<?php

use Illuminate\Database\Seeder;
use Yeltrik\University\app\models\Dean;

class DeanSeeder extends Seeder
{
    use \App\AsanaExportSeeding_Trait;
    use \App\AsanaTaskCustomField_Trait;

    const ASANA_JSON_EXPORT_PATH = 'dean.json';

    const USER_PASSWORD = 'WkuCitl20!';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(Storage::disk('public')->get(static::ASANA_JSON_EXPORT_PATH), true);

        foreach ($json as $data) {
            foreach ($data as $key => $task) {
                $name = $task['name'];
                $customFields = $task['custom_fields'];
                $asanaGid = $task['gid'];
                $email = strtolower(static::getCustomFieldTextValue($customFields, 'Email'));
                $wkuId = static::getCustomFieldNumberValue($customFields, 'WKU ID');
                $netId = static::getCustomFieldTextValue($customFields, 'NetID');
                //$teachingStatus = static::getCustomFieldEnumValue($customFields, 'Teaching Status');
                //$facultyRank = static::getCustomFieldEnumValue($customFields, 'Faculty Rank');
                //$highestDegree = static::getCustomFieldEnumValue($customFields, 'Highest Degree');
                $collegeName = static::getCustomFieldEnumValue($customFields, 'College');
                //$departmentName = static::getCustomFieldEnumValue($customFields, 'Department');

                $college = $this->getCollege($collegeName);
                //$department = $this->getDepartment($departmentName, $college);

                if ($email != NULL) {
                    $user = $this->getUser($name, $email, static::USER_PASSWORD);
                    $wkuIdentity = $this->getWkuIdentity($user, $name, $email, $netId, $wkuId, $asanaGid, $college);

                    // Create Dean
                    $dean = new Dean();
                    $dean->college()->associate($college);
                    $dean->wkuIdentity()->associate($wkuIdentity);
                    $dean->save();

                    //$this->command->info("Dean: " . $name);
                } else {
                    $this->command->warn('Skipping Dean: ' . $name);
                }
            }
        }
    }
}
