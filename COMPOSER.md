#Composer

## Setup

### Initial Branch
Create an orphan branch in an existing repo for a Composer package

cd {repository}  
git checkout --orphan composer  
git rm -rf .  
rm '.gitignore'  
echo "#Composer" > COMPOSER.md  
git add COMPOSER.md  
git commit -a -m "Initial Commit"  
git push origin composer

### GitLab

* Settings 
  * Repository
    * Default Branch
      * composer  
    * Protected Branches
      * composer : maintainers : maintainers

### Composer

Initialize and Setup a Composer package.

* composer init
  * Package name: yeltrik/package-name
  * License: MIT
  * vendor added to .gitignore: yes
* commit & push

### Packagist Discovery and Integration

* https://packagist.org/packages/submit
  * Paste git package url
  * Click Auto-Updated to access Token
* gitlab
  * Settings
    * Integrations
      * ...
  
